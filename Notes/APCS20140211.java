//Vincent Ou
//APCS2 pd 8
//2014-02-11

/*
Terminal Commands

pwd- print working directory
ls - lists all the items
ls -l - lists all as a list with more information
cd - change directory
cd /
cd ~
cd ..
cd ../..
cd ~/Downloads

CLI - Command Line Interface

ctrl - l - clear terminal

cat <filename> - see text
more <filename> - see text
less <filename> - see text

mkdir foo
rmdir foo
touch foo - timestamp - if file does not exist
rm foo
rm -r
rm -rf

cp
mv





