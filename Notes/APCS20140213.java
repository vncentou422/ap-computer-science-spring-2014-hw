//Vincent Ou
//APCS2 pd 8
//2014-02-13

/*
Aim: Recursion Again

byte 8 bits
short 16 bit         
int 32 bit           float 32 bit
long 64 bit          double 64 bit

2
_ 
1 bit

32 bit
2^32 unique values represented by a 32 bit

2^31 -1 because
negatives
2^32/2 = 2^31
[2^31 values on the left, 0 , 2^31-1]