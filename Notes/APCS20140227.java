//Vincent Ou
//APCS2 pd 8
//2014-02-27

/*
Aim: Chess Tour

GRAPH THEORY
THE TRAVELING SALESMAN PROBLEM
shortest way to get a certain city

o----------------o-----------o
 -              -          -     
  -        -            -
   -    -             - ^edge
    - -            - 
     o----------o -
      -         -
       -       -
        -    -
          o
          ^nodes

This is a graph as well


tour: path that visits every location exactly once (aka Hamiltonian)
closed tour: begin and end at same location	(aka Hamiltonian cycle)
open tour: begin and end at different location  (aka Hamiltonian path)
