/*
Aim:
Do Now:

Java passes args by value

if arg is a primitive, pass its value (ie a "copy")
if arg is an object, pass its value (ie, a point)
Arrays qualify as Objects here


public static void foo (int [] endymion){
    for (int i = 0; i < endymion.length; i++){
	endymion[i] = 0;
    }
}
*/