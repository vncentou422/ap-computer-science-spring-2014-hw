/*
Aim:
Do Now:

Update MergeSort to work with ArrayLists
typed for integers
with & w/o return value

public static void
MergeSortV(ArrayList<Integer> data){}

and a returning version

public static ArrayList<Integer>
MergeSort(ArrayList<Integer> data){}

measure execution times, average over batches for each flavor

upload individually to HW server

*/