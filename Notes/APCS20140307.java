/*
Aim: Never fear, Big Oh is here

ArrayList names = new ArrayList();
ArrayList<Object> name = new ArrayList<Object>();

1000 data set
                 Best                  Worst
Linear Search    1(first element)      1000 (not in data set/last element)
Binary Search    1(middle element)     10(log(base2)n) 

Linear Search    O(1)                  O(n) linear
Binary Search    O(1)                  O(log(base 2) n) logarithmic

f is Big Oh of ___

"f(n) is O(g(n))" ... if f(n) <= C * g(n);

... for some C & k where n > k

Read as 
"f is Big Oh of g(n)" or "f is on the order of g or n"