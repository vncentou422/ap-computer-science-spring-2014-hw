/*
Aim: A carnival game
DN: Why is Big Oh useful?
Boss - Estimated Runtime?
Platform on which code runs
Optimize runtime

N: Big O notation allows for platform-independent algorithm efficiency comparison.

If you have a function, f, representing number of operations as a fxn of n, you can say...

"f is Big Oh of  ____"
or
"f is on the order of ____"
or
"f is bounded by ____"

...where the ____ is bounding function

f(n) is O(g(n)) 
                iff
		     f(n) <= C * g(n)

...for some C & k where n > k
make k = 1 to make sure it doesn't affect the log statment because if multiplied by a constant, it will further digress into the negatives

Read as
"f is Big Oh of g of n"
or
"f is on the order of g of n"

Jot down a f(n) representation of yoo();
*/
public int yoo( ){
    int aoo; //1
    int boo; //1
    int coo; //1
    aoo = 5; //1
    boo = 5 + 7; //2
}

/*
constant time because doesn't depend on any set
f(n) = 1+1+1+1+2 = 6
g(n) = 1
C = 6
k = 0
f(n) <= 6g(n) for n > 0

e.g., a function accessing an element of an array and ussing stuff

*/
public int foo(int x){
    int boo = data[x] + 20140310/23;
}
/*
f(n) = 1 +4
so 
f(n) is O(1);
*/

public int zoo(int x){
    int boo = data[x] + 20140310/23;
    //1     1    1     1     1 = 5 
    for (int i: data){
	boo = i*2 + 3;
	//1    1    1 = 3
 	System.out.println("Feet hats FTW");//1
    }
}

/*
may be represented in shorthand as 
f(n) = ?
f(n) is O(?)
for C = ? and k = ?

f(n) = 4*n + 5
f(n) is O(n)
4n+5 <= C(g(n)
n > k
C = 4//5
k = 5//0

Rules of thumb;
O(n): algorithm varies directly with n
O(n) & O(log(base2) n) equivalent for small n
O(log(base2) n) means fast
we generally measure time in number of passes through a dataset
ignore constants
look for governing(highest order) term

4n  + 5 --> O(n)
X     X

ORDER    NAME                             N = 1     N = 10          N = 100          N=1000
O(1)    "constant"                         1          1                1                1
O(logn) "logarithmic"                      1          3ish             6ish             9
O(n)    "linear"                           1          10               100              1000
O(nlogn) "loglinear"/                      1          33ish            664ish           9970
         "linear logarithmic"
	 "linearthmic"

Q(n^2)   "quadratic"                       1          100              10000            1,000,000

HOMEWORK
*/