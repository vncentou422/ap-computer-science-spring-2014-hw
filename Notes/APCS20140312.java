public class APCS20140312{

    public static final String ALPHABET = "abcdefghijklmnopqrstuvwxyz";
    public static String makeString(int length){
	String str = "";
	for (int i = 0; i < length; i++){
	    int randLetter = (int)(Math.random()*25);
	    str = str + ALPHABET.substring(randLetter, randLetter+1);
	}
	return str;
    }
    public static String reverse1(String s){
	String retStr = "";
	for(int i = s.length() - 1; i > -1; i--){
	    retStr += s.substring(i, i+1);
	}
	return retStr;
    }
    public static String reverse2(String s){
	if (s.length() == 1){
	    return s;
	}
	else{
	    String a = s.substring(0, s.length() / 2);
	    String b = s.substring(s.length() / 2);
	    return reverse2(b) + reverse2(a);
	}
    }
    public static void main (String [] args){
	String str = makeString(5);
	//System.out.println(str);
	//System.out.println(reverse1(str));
	System.out.println(reverse2(str));

    }
}

/*
Notes
O(2logn) -> O(logn)


desserts = 8
dess erts
de ss er ts
d e s s e r t s
ed ss re st
ssed stre
stressed

So...
To reverse a String...
a) O(n) is possible
b) O(logn) is impossible because you can't ignore/cut out half of the data, you must touch each letter every time;

*/