public class APCS20140317{
    /*
      desserts
      erts   dess
      ts er  ss  de
      s t r e s s e d
      st re ss ed
      stre ssed
      stressed

Aim: Select. And Quickly!
DN: Reboot, turn on brain

Q: What was the runtime of your "optimized" string reverser? nlogn

find smallest number in set - O(n) - reigning champion algorithm
find kth smallest - sort + kth index - (depends on the sort) + O(1) pass
                                     - O(nlogn) + O(1) -> O(nlogn)
An algo for: Find and remove smallest, k times
[7,1,5,12,3]
[1,5,3,7,12]
       ^sorted or in final resting position
1
1,5,3,7,12
^know that this is in FRP
5
1,3,5,7,12
5 = FRP
3 = FRP
1,3,5,7,12 = sorted

TASK: Write class QuickSelect to implement our algo:
QSel(arr, k, lo, hi):
   IF lo >= hi
        kth val has been located
   ELSE
        a) select pivot b/t lo, hi
	b) partition A around pivot
	c) if pivot val is at final dest, we're done else Qsel (arr, k, new _lo, new _hi)

And answer these Q's;
1) Worst pivot? Associated runtime?
2) Best pivot? Associated runtime?
