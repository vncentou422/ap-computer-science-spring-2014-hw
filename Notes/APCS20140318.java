public class APCS20140318{
    public static int foo(int min, int max, int[] d){
	
	return fooHelp(0, d.length-1, min, max, d);
    }
    public static int[] foox(int min, int max, int[] d){
	
	return fooHelpp(0, d.length-1, min, max, d);
    }
    public static int fooHelp(int lo, int hi, int min, int max, int[] d){
	while (lo < hi){
	    if (d[lo] == max){
		swap (lo, hi--, d);
	    }
	    else
		lo++;
	}
	if (d[hi] == max)
	    hi--;
	return hi;
    }
    public static int[] fooHelpp(int lo, int hi, int min, int max, int[] d){
	while (lo < hi){
	    if (d[lo] == max){
		swap (lo, hi--, d);
	    }
	    else
		lo++;
	}
	if (d[hi] == max)
	    hi--;
	return d;
    }
    public static void swap(int y, int z,int[] x){ //swap method
	int temp = x[y];
	x[y] = x[z];
	x[z] = temp;
    }
    public static String print(int[] x){
	String retStr = "[";
	for (int c: x){
	    retStr += c + ",";
	}
	return retStr.substring(0, retStr.length() - 1) + "]";
    }
    public static void main(String []args){
	int[] b = {1,0,0,1,1,0,1,0};
	System.out.println(foo(0,1,b));
	System.out.println(print(foox(0,1,b)));
    }
    //int[] t = {2,1,0,2,0,1,0,2};
    //partition3Vals(0,1,2,t) -> t is now {0,0,0,1,1,2,2,2}
    public static void partition3Vals(int min, int mid, int max, int[] d){
        int hi = partition(min, max, d);
	partitionHelp(0,hi,min, mid, d);
    }
}