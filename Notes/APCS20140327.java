public APCS20140327{

    /*
      Aim: ?
      DN: ?
      
      Q: what did this error indicate, and how did you resolve it?

      $ javac ListTester.java
      ListTester.java:44: add(java.lang.String) in List<java.lang.String> cannot be applied to (int, java.lang.String)
               wu.add(n, "@");
                 ^
      ListTester.java:51: cannot find symbol
      symbol: method remove(int)
      location: interface List<java.lang.String>
               wu.remove(n);
	         ^
      2 errors


      A: 
         T t = new T();
	 ^variable ^ object
	 
	 Obj -> Human -> Student-> AnneDuncan
	 Student ad = new AnneDuncan();

	 List<String> wu = new LList<String>();
	 ^wu registered as a List
	 
	 therefore functions add and remove are not added into List because java registers wu as a List
	 
	 

=============================================================  DLLNODE ========================================================================   
public class DLLNode<T> {

    private T _cargo;    //cargo may only be of type T
    private DLLNode<T> _nextNode; //pointer to next DLLNode
    private DLLNode<T> _prevNode;


    // constructor -- initializes instance vars
    public DLLNode( T value, DLLNode<T> next, DLLNode<T> prev ) {
	_cargo = value;
	_nextNode = next;
	_prevNode = prev;
    }


    //--------------v  ACCESSORS  v--------------
    public T getCargo() { return _cargo; }
    public DLLNode<T> getPrev() {return _prevNode; }
    public DLLNode<T> getNext() { return _nextNode; }
    //--------------^  ACCESSORS  ^--------------


    //--------------v  MUTATORS  v--------------
    public T setCargo( T newCargo ) {
	T foo = getCargo();
	_cargo = newCargo;
	return foo;
    }

    public DLLNode<T> setNext( DLLNode<T> newNext ) {
	DLLNode<T> foo = getNext();
	_nextNode = newNext;
	return foo;
    }

    public DLLNode<T> setPrev( DLLNode<T> newPrev){
	DLLNode<T> foo = getPrev();
	_prevNode = newPrev;
	return foo;
    }
    //--------------^  MUTATORS  ^--------------


    // override inherited toString
    public String toString() { return _cargo.toString(); }


    //main method for testing
    public static void main( String[] args ) {

	//Below is an exercise in creating a linked list...

	//Create a node
	DLLNode<String> first = new DLLNode<String>( "cat", null, null );

	//Create a new node after the first
	first.setNext( new DLLNode<String>( "dog", null , first) );

	//Create a third node after the second
	first.getNext().setNext( new DLLNode<String>( "cow", null , first.getNext()) );

	DLLNode temp = first; 
	while( temp != null ) {
	    System.out.println( temp );
	    temp = temp.getNext();
	}

    }//end main

}//end class DLLNode
=============================================================  LLIST ========================================================================
see file posted by Brown


	
   */
}
