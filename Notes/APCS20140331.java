public APCS20140328{

    /*
      Aim: Let me reiterate
      DN: What is wrong with this?
      
      public static List<Integer> oddsB(List<Integer> L){
             List<Integer> retL = new ArrayList<Integer>();
	     Iterator itr = L.iterator();
	     while( itr.hasNext() ){
	          Integer tmp = itr.next();
		  if (tmp % 2 == 1)
                     retL.add(temp);
	     }
             return retL;
      }

      A: itr.next() returns an Object
      FIX: typecast return value into an Integer, or make itr an Iterator<Integer>
      Iterator<Integer> itr = L.iterator();
      
      public static List<Integer> oddsB(List<Integer> L){
             List<Integer> retL = new ArrayList<Integer>();
	     Iterator<Integer> itr = L.iterator();
	     while( itr.hasNext() ){
	          Integer tmp = itr.next();
		  if (tmp % 2 == 1)
                     retL.add(temp);
	     }
             return retL;
      }
      
      
      
      List<String> Barak = ["Jia","Jim","Rebecca"]
      
      
      first -> |Jia| -> |Jim| -> |Rebecca|
               | --|--  | --|--  |   ----|----> null
       null < -|-- |  <-|-- | <--|---    |

       DLLNode _dummy = null;


       ========================Nested Classes================================
       
       class Outerclass{
             ...
	     class NestedClass{
	           ...
	     }
       }


       Why Use Nested Classes?
       -Logicallly group classes that are only used in one place: If a class is useful to only one other class, then it is logical to embed it in that class and keep the two together. Nesting such "helper classes" makes their package more streamlined
       -Increase encapsulation: Consider two top level classes, A and B,where B neeeds access to members of A that would otherwise be declared private. By hiding class B within class A, A's members can be declared private and B can access them. In addition.......


       2flavors of nested classes: static and non stati
       Nested classes that are declared static are called static nested classes
       Non static nested classes are called inner classes
       
       class OuterClass{
             ...
	     static class StaticNestedClass{
	           ...
	     }
	     class InnerClass{
	           ...
	     }
       }

       A nested class is a member of its enclosing class. Non static nested classes(Inner classes) have access to other members of the enclosing class, even if they are declaerd private. Static nested classes do not have access to other members of the enclosing class. As a member of the OuterClass, a nested class can be declared private, public, protected, or package private( Recall taht outer classes can only be declared public or package private.)
       
       todd.iterator();
       LList.iterator();



       

       //dummy node for tracking current pos
       private DLLNode<T> _dummy;
       
       public MyIterator(){}
       public boolean hasNext(){}
       public T next() {}
       public void remove(){}
              must take into acct:
	      next() call MUST precede each remove() call
	      eg net(), rm(), rm() -> MUST THROW EXCEPTION
	      Q: how make sure usser calls next() before rm?



       
	
   */
}
