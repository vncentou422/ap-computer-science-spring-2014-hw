public APCS20140423{
    /*
      A new abstract data type (ADT):
      N: A queue is a collection with the FIFO property.
      FIFO:?
      Stack: LIFO-last in first out
      QUEUE: FIFO-first in first out
      
      Q: What should a queue be able to do?
      add to collection
      remove from collection
      peek
      IsEmpty()