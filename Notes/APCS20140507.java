public APCS20140507{
    /*
      tree: undirected graph wherein any 2 vertices are connected by exactly 1 path
      tree: graph with no tours

      rooted tree: tree in whcih one node has been designated the root
      (random)

      PARENT: connected node directly above a gvn node
      CHILD: connected node directly below a gvn node
      SIBLINGS: nodes w/ same parent node
      
      ROOT: topmost node(by def has no parent)
      LEAF: node w/ no children
      SUBTREE: group of connected nodes w/in a tree
      LEVEL: # of nodes fr a gvn node to root
      HEIGHT: max level + 1

      BALANCED: height of left & right subtrees of any node differs by 1 or less
      COMPLETE: every level filled in, in a left-justified manner, before moving to a next step
      PERFECT: every level is full

      Q: All perfect trees complete?
      Yes
      Q: All complete trees balanced?
      Yes
      Q: What must be true of perfect trees but not others?
      x
    */