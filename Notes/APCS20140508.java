public APCS20140508{
    /*
      A 2node tree is perfect when it has 2^h - 1 nodes
      
      Q: How does one traverse a tree?
      Process left subtree, process right subtree

      3 types of traversals
      pre-order 
      in-order  
      post-order
      
               A
	      / \
	     B   C
	    /\    \
	   D  E    F
	   
      pre order - ABDECF
      in order DBEACF
      post-order DEBFCA

      pre order - Process recurse down left, recurse down right
      in order - Recruse left, process, recurse left
      post order - recurse left, recruse right, process
      Q: what does this mean?

      pre-order process myself and then process subtrees

      prefix indicates when root node is processed
      N: each non-leaf node in a tree is also a subtree root
      
      Pre-order
      Visit the root.
      Traverse the left subtree.
      Traverse the right subtree
      In-order 
      Traverse the left subtree.
      Visit the root.
      Traverse the right subtree.
      Post-order
      Traverse the left subtree.
      Traverse the right subtree.
      Visit the root.

      For a given tree, how many unique pre traversals exist?
      
      in-?
      post-?

      A: 1 of each
      
      

      Given traversal ABCDEF
      
      A: Many possible trees
