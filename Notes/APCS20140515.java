/*

  AIM: Firefighters and kitties

  DN: HEMLOCK
  inorder KCOLMEH
  preorder KCOLMEH

  Permission Slip for Stephen Wolfram 
  
  use of helper functions to help with recursion
  analysis of preOrderTrav functions
  
  toString()
  return type of traversals to string
  return string += return value;
  
  
  insertion
  
  Rothman:
  basic insert - int value if if the node is empty u just put it in there
  not u call the second insert method

  second insert method takes subtree root and new node
  compare values, check if left is empty -> insert else recurse
  compare values for right etc same
  
  (assume unique values)
  
  
  Q: How does one remove from a BST?
  
  Q: Possible cases?
  Interior node vs a leaf vs a root
  How to remove 100

           100
	  /    \
        50     150
         \
         75
         / \
        60  90

  make 150 the new root

  can swap 100 and 150 but we don't know the structure of tree
  
  search down the left subtree for greatest value and swap 100
    recursive call
    continue as far as i can to the right
    pull high value to the top
    pull root of left subtree to the root 
    
  
  make 50 the new root (could but it'll be alot of work)
  go to 50, if no left subtree
  lean right - find lowest value/leftmost value

  search function
  90
  ami90? no? less? traverse left greater? traverse right
  

  "piggybacking" putting in a parent and child node

  HW: new BST.java in closet with search, remove, height, 
  