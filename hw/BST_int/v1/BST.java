//Vincent Ou
//APCS pd 8
//HW37
//2014-05-09

/*****************************************************
 * class BST - skeleton
 * Implementation of the BINARY SEARCH TREE abstract data type (ADT) 
 * This BST only holds ints (its nodes have int cargo)
 *****************************************************/

public class BST {

    //instance variables / attributes of a BST:
    private TreeNode root;

    /*****************************************************
     * default constructor
     *****************************************************/
    BST( ) {
	root = null;
    }


    /*****************************************************
     * void insert( int ) 
     * Adds a new data element to the tree at appropriate location.
     *****************************************************/
    public void insert( int newVal ) {
	//TreeNode tmp = root;
        if (root == null)
	    root = new TreeNode(newVal);
	else{
	    TreeNode x = null;
	    TreeNode tmp = root;
	    while (tmp != null){
		if(newVal < tmp.getValue()){
		    x = tmp;
		    tmp = tmp.getLeft();
		}
		else if ( newVal > tmp.getValue()){
		    x = tmp;
		    tmp = tmp.getRight();
		}
		else{
		    System.out.println("no duplicates");
		}
	    }
	    if (newVal < x.getValue())
		x.setLeft(new TreeNode(newVal));
	    else
		x.setRight(new TreeNode(newVal));
	}
	

    }
    //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    //~~~~~~~~~~~~~v~~TRAVERSALS~~v~~~~~~~~~~~~~~~~~~~~~
    public void preOrderTrav() 
    {
        TreeNode tmp = root;
	preOrderTravHelp(tmp);
    }
    public void preOrderTravHelp(TreeNode tmp){
	if ( tmp != null){
	    System.out.println(tmp.getValue());
	    preOrderTravHelp(tmp.getLeft());
	    preOrderTravHelp(tmp.getRight());
	}
    }

    public void inOrderTrav() 
    {
        TreeNode tmp = root;
	inOrderTravHelp(tmp);
    }
    public void inOrderTravHelp(TreeNode tmp){
	if ( tmp != null){
	    
	    inOrderTravHelp(tmp.getLeft());
	    System.out.println(tmp.getValue());
	    inOrderTravHelp(tmp.getRight());
	}
    }
    public void postOrderTrav() 
    {
	TreeNode tmp = root;
	postOrderTravHelp(tmp);
    	
    }
    public void postOrderTravHelp(TreeNode tmp){
	if ( tmp != null){
	    
	    postOrderTravHelp(tmp.getLeft());
	    postOrderTravHelp(tmp.getRight());
	    System.out.println(tmp.getValue());
	}
    }
    //~~~~~~~~~~~~~^~~TRAVERSALS~~^~~~~~~~~~~~~~~~~~~~~~
    //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~


    //main method for testing
    public static void main( String[] args ) {

	
	BST arbol = new BST();

	arbol.insert( 4 );
	arbol.insert( 2 );
	arbol.insert( 5 );
	arbol.insert( 6 );
	arbol.insert( 1 );
	arbol.insert( 3 );
	
	
	System.out.println( "\npre-order traversal:" );
	arbol.preOrderTrav();

	System.out.println( "\nin-order traversal:" );
	arbol.inOrderTrav();
	
	System.out.println( "\npost-order traversal:" );
	arbol.postOrderTrav();	

    }

}//end class





    
