//Vincent Ou
//APCS pd 8
//HW#38
//2014-05-13

/*****************************************************
 * class BST
 * Implementation of a BINARY SEARCH TREE
 * A BST maintains the invariant that, for any node N with value V, 
 * L<V && V<R, where L and R are node values in N's left and right
 * subtrees, respectively.
 * This BST only holds ints (its nodes have int cargo)
 *****************************************************/

public class BST {

    //instance variables
    TreeNode _root;
    TreeNode _parentNode;

    /*****************************************************
     * default constructor
     * Initializes an empty tree.
     *****************************************************/
    BST( ) {
	_root = null;
	_parentNode = null;
    }
    
    TreeNode getRoot(){
	return _root;
    }

    /*****************************************************
     * void insert( int ) 
     * Adds a new data element to the tree at appropriate location.
     *****************************************************/
    public void insert( int newVal ) {

	TreeNode newNode = new TreeNode( newVal );

	if ( _root == null ) {
	    _root = newNode;
	    return;
	}
        insert( _root, newNode );
    }
    //recursive helper for insert(int)
    public void insert( TreeNode stRoot, TreeNode newNode ) {

	if ( newNode.getValue() < stRoot.getValue() ) {
	    //if no left child, make newNode the left child
	    if ( stRoot.getLeft() == null )
		stRoot.setLeft( newNode );
	    else //recurse down left subtree
		insert( stRoot.getLeft(), newNode );
	    return;
	}
	else { // new val >= curr, so look down right subtree
	    //if no right child, make newNode the right child
	    if ( stRoot.getRight() == null )
		stRoot.setRight( newNode );
	    else //recurse down right subtree
		insert( stRoot.getRight(), newNode );
	    return;
	}
    }



    /*****************************************************
     * TreeNode search(int)
     * returns pointer to node containing target,
     * or null if target not found
     *****************************************************/
    TreeNode search( int target )
    {
        if (_root == null)
	    return null;
	else
	    return search(_root, target);
    }
    TreeNode search(TreeNode root, int target){
	/*
	if (root.getRight() == null && root.getLeft() == null)
	    return null;
	else 
	*/
	if(root.getValue() == target)
	    return root;
	else if (root.getRight() == null && root.getLeft() == null)
	    return null;
	else if( target > root.getValue())
	    return search(root.getRight(), target);
	else //if (target < root.getValue())
	    return search(root.getLeft(), target);
        
    }
    TreeNode searchP( int target )
    {
        if (_root == null)
	    return null;
	else
	    return searchP(_root, target);
    }
    TreeNode searchP(TreeNode root, int target){
	if(root.getLeft().getValue() == target || root.getRight().getValue() == target)
	    return root;
	/*
	else if (root.getRight() == null && root.getLeft() == null)
	    return null;
	*/

	else if( target > root.getLeft().getValue() || target > root.getRight().getValue())
	    return searchP(root.getRight(), target);
	else if (target < root.getValue())
	    return searchP(root.getLeft(), target);
	else 
	    return null;
    }
	
    /*****************************************************
     * void remove( int )
     * if remVal is present, removes it from tree
     * Assumes no duplicates in tree.
     *****************************************************/
    public void remove( int remVal )
    {
        remove(_root, remVal);
    }
    public TreeNode findMax(TreeNode root){
	if(root.getRight() == null){
	    return root;
	}
	else
	    return findMax(root.getRight());
    }
    public void remove( TreeNode root, int remVal){
	TreeNode removee = search(remVal);
	TreeNode removeeP = searchP(remVal);
	TreeNode replace = findMax(removee);
	TreeNode replaceP = searchP(replace.getValue());
	if (removeeP != null){
	    replace.setLeft(removee.getLeft());
	    replace.setRight(removee.getRight().getRight());
	    removeeP.setLeft(replace);
	}
	else{
	    replace.setLeft(removee.getLeft());
	    replace.setRight(removee.getRight());
	    replaceP.setRight(null);
	}

    }


    /*****************************************************
     * int height()
     * returns height of this tree (length of longest leaf-to-root path)
     * eg: a 1-node tree has height 1
     *****************************************************/
    public int height()
    {
	return height(_root);
    }
    public int height(TreeNode root){
	if (root == null)
	    return 0;
	else{
	    int right = 0;
	    int left = 0;
	    right = height(root.getRight());
	    left = height(root.getLeft());
	    return Math.max(right,left) + 1;
	}
    }
    


    /*****************************************************
     * int numLeaves()
     * returns number of leaves in tree
     *****************************************************/
    public int numLeaves()
    {
	return numLeaves( _root);
	
    }
    public int numLeaves(TreeNode root){
	if (root == null)
	    return 0;
	else if (isLeaf(root))
	    return 1;
	else
	    return numLeaves(root.getLeft()) + numLeaves(root.getRight());
    }
    public boolean isLeaf( TreeNode x ){
	if( x.getRight() == null && x.getLeft() == null)
	    return true;
	return false;
    }

    //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    //~~~~~~~~~~~~~v~~TRAVERSALS~~v~~~~~~~~~~~~~~~~~~~~~
    //process root, recurse left, recurse right
    public void preOrderTrav() {
	preOrderTrav( _root );
    }
    public void preOrderTrav( TreeNode currNode ) {
	if ( currNode == null )
	    return;
	System.out.print( currNode.getValue() + " " );
	preOrderTrav( currNode.getLeft() );
	preOrderTrav( currNode.getRight() );
    }

    //recurse left, process root, recurse right
    public void inOrderTrav() {
	inOrderTrav( _root );
    }
    public void inOrderTrav( TreeNode currNode ) {
	if ( currNode == null )
	    return;
	inOrderTrav( currNode.getLeft() );
	System.out.print( currNode.getValue() + " " );
	inOrderTrav( currNode.getRight() );
    }

    //recurse left, recurse right, process root
    public void postOrderTrav() {
	postOrderTrav( _root );
    }
    public void postOrderTrav( TreeNode currNode ) {
	if ( currNode == null )
	    return;
	postOrderTrav( currNode.getLeft() );
	postOrderTrav( currNode.getRight() );
	System.out.print( currNode.getValue() + " "  );
    }
    //~~~~~~~~~~~~~^~~TRAVERSALS~~^~~~~~~~~~~~~~~~~~~~~~
    //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~


    //main method for testing
    public static void main( String[] args ) {

	BST arbol = new BST();
	arbol.insert( 4 );
	arbol.insert( 2 );
	arbol.insert( 5 );
	arbol.insert( 6 );
	arbol.insert( 1 );
	arbol.insert( 3 );

	System.out.println( "\npre-order traversal:" );
	arbol.preOrderTrav();

	System.out.println( "\nin-order traversal:" );
	arbol.inOrderTrav();
	
	System.out.println( "\npost-order traversal:" );
	arbol.postOrderTrav();	

	System.out.println();

	System.out.println(arbol.numLeaves());
	System.out.println(arbol.height());
	System.out.println(arbol.search(2).getValue());  //2
	System.out.println(arbol.searchP(2).getValue()); //4
	//System.out.println(arbol.searchP(4).getValue()); //null
	System.out.println(arbol.searchP(1).getValue()); //2
	System.out.println(arbol.searchP(3).getValue()); //2
	
	System.out.println(arbol.findMax(arbol.getRoot()).getValue()); //6
	System.out.println(arbol.findMax(arbol.search(2)).getValue()); //3
	int x = arbol.findMax(arbol.search(2)).getValue();
	System.out.println(arbol.searchP(x).getValue()); //2
	
	
	arbol.remove(2);
	System.out.println( "\npre-order traversal:" );
	arbol.preOrderTrav();

	System.out.println( "\nin-order traversal:" );
	arbol.inOrderTrav();
	
	System.out.println( "\npost-order traversal:" );
	arbol.postOrderTrav();

	System.out.println();

	/*~~~~~~~~~~~~move~me~down~~~~~~~~~~~~~~~~~~~~~~
	~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~*/
    }

}//end class





    // /*****************************************************
    //  * 
    //  *****************************************************/
    // (  )
    // {
    // 	/*** YOUR IMPLEMENTATION HERE ***/
    // }
