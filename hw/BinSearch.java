//Vincent Ou
//APCS2 pd 8
//2014-03-11
//HW12

/*==================================================
  class BinSearch -- Implements binary search algorithm on an int array.
  Team Sac Ou Victory (Isaac Gluck, Victor Gaitour, Vincent Ou)  

  Summary of Binary Search Algorithm:
  BinarySearch(recursive) first finds the midpoint of the arrays you are searching in. Basically, using lo and hi to indicate where the area you are searching for starts and ends, you would find the midpoint by averaging the lo and hi. Then you take that value at that inde and you would see if it matches the target value. If it is higher than the target value, you would se the hi to the current halfed value + 1 while if it is lower then you would set it th elo to the half + 1. Otherwise, if lo == hi, then the list has been searched through so you would return a -1, otherwise the half value if the target is found. Binsearch(iterative) basically follows the same principle, except you must instigate a separate variable in order to break out of the while loop. 

Runtime: Given : 60 ms
100: 61 ms
1000: 135 ms
10000: 484 ms
100000: ---


Runtime: O(logn)


  ==================================================*/

public class BinSearch {


    /*==================================================
      int binSearch(int[],int) -- searches an array of ints for target int
      pre:  input array is sorted in ascending order
      post: returns index of target, or returns -1 if target not found
      ==================================================*/
    public static int binSearch ( int[] a, int target ) {
	//uncomment exactly 1 of the 2 stmts below:
	//return binSearchIter( a, target, 0, a.length-1 );
	return binSearchRec( a, target, 0, a.length-1 );
    }


    public static int binSearchRec( int[] a, int target, int lo, int hi ) {	
	int x = (lo + hi)/2;
	if (lo == hi){
	    return -1;
	}
	else if (a[x] < target){
	    lo = x + 1;
	    return binSearchRec( a, target, lo, hi);
	}
	else if (a[x] > target){
	    hi = x + 1;
	    return binSearchRec( a, target, lo, hi);
	}
	else
	    return x;
    }


    public static int binSearchIter( int[] a, int target, int lo, int hi ) {
        int x = -1;
	while (!(lo == hi)){
	    int y = (lo + hi)/2;
	    if (a[y] == target){
		x = y;
		break;
	    }
	    else if( a[y] < target){
		lo = y + 1;
	    }
	    else if (a [y] > target){
		hi = y + 1;
	    }
	}
	return x;
    }



    //tell whether an array is sorted in ascending order
    private static boolean isSorted( int[] arr ) {
	for (int x = 0; x < arr.length -1; x++){
	    if (arr[x] > arr[x+1]){
		return false;
	    }
	}
	return true;
    }


    // utility/helper fxn to display contents of an array of Objects
    private static void printArray( int[] arr ) {
	String output = "[ "; 

	for( int i : arr )
	    output += i + ", ";

	output = output.substring( 0, output.length()-2 ) + " ]";

	System.out.println( output );
    }


    //main method for testing
    public static void main ( String[] args ) {

	
	int n = Integer.parseInt(args[0]);
	
	//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
	System.out.println("\nNow testing binSearch on int array...");

	//Declare and initialize array of ints
	int[] iArr = { 2, 4, 6, 8, 6, 42 };
	printArray( iArr );
	System.out.println( "sorted? -- " + isSorted(iArr) );

	//int[] iArr2 = { 2, 4, 6, 8, 13, 42 };
	int[] iArr2 = new int[n];
	for (int x = 0; x < n; x++){
	    iArr2[x] = x;
	}
	printArray( iArr2 );
	System.out.println( "sorted? -- " + isSorted(iArr2) );

	//search for 6 in array 
	//System.out.println( binSearch(iArr,6) );

	//search for 43 in array 
	System.out.println( binSearch(iArr,1000000000) );
	//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
/*==================================================
	==================================================*/

    }//end main()

}//end class BinSearch
