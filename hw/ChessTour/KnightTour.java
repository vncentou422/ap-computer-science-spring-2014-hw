//Vincent Ou
//APCS2 - pd 8
//HW#7
//2014-03-03

/*======================================
  class KnightTour
  Animates a Knight's Tour of a square chess board.

  Mean execution times for boards of size n*n:
  n=5   23.121s  over 3  executions 
  n=6   7251_s   over 3  executions
  n=7   est about 20+ hours      over x  executions
  n=8   est about 20+ hours      over x  executions
  =====================================*/

//Q1: From any starting position, can a tour always be found? Explain.
//No because there are cases where it is physicaly impossible for the knight to transverse all the patches because the knight only has a limited way to reach each patch.
//Q2: How do you explain the increase in execution time as n increases?
//As the size of the board increases, the knight can't land on a place that it has already been in, so it has to recursively find a unique way to traverse the entire board, landing on each piece exactly once, thus the long runtime.
import java.io.*;
import java.util.*;

class KnightSolver {
    private int[][] board;
    private int sidelength;
    private boolean full;
    private int num;

    public KnightSolver(){
	num = 64;
	full = false;
	sidelength = 8;
	board = new int[sidelength+4][sidelength+4];
	populate();
	System.out.println(board);
    }
    public KnightSolver(int x ){
	full = false;
	num = x * x;
	sidelength = x;
	board = new int[sidelength+4][sidelength+4];
	populate();
	System.out.println(board);
    }
    
    private void delay( int n ) {
	try {
	    Thread.sleep(n);
	}
	catch( InterruptedException e ) {
	    System.exit(0);
	}
    }
    public int[][] getBoard(){
	return board;
    }
    public void populate(){
        for (int x = 0; x < sidelength+4; x++){
	    for (int c = 0; c < sidelength+4; c++){
		board[x][c] = -1;
	    }
	}
	for (int x = 2; x < sidelength +2; x++){
	    for (int c = 2; c < sidelength +2; c++){
		board[x][c] = 0;
	    }
	}
    }
	
		
		
    public String toString() {
	//send ANSI code "ESC[0;0H" to place cursor in upper left
	String retStr = "[0;0H\n";  
	//emacs shortcut: C-q, then press ESC
	//emacs shortcut: M-x quoted-insert, then press ESC
	// (alternatively, just copy and paste from this file...)

	int i, j;
	for (i = 0; i < sidelength+4 ; i++){
	    for ( j = 0; j < sidelength+4;j++){
		retStr = retStr + String.format( "%3d" , board[j][i]);
	    }
	    retStr = retStr + "\n";
	}
	return retStr;
    }
    public void findTour(int x, int y,int  n){
	//delay(50);
	if (full) System.exit(0);
	
	if ( n > sidelength * sidelength  ){
	    full = true;
	    System.out.println(this);
	    return;
	}
	else if ( board[x][y] >= 1 || board[x][y] == -1){
	    return;
	}
	else{
	    board[x][y] = n;
	    System.out.println(this);
	    findTour(x + 2, y + 1, n + 1);
	    findTour(x + 2, y - 1, n + 1);
	    findTour(x + 1, y - 2, n + 1);
	    findTour(x + 1, y + 2, n + 1);
	    findTour(x - 2, y+1, n + 1);
	    findTour(x - 2, y-1, n + 1);
	    findTour(x - 1, y-2, n + 1);
	    findTour(x - 1, y+2, n + 1);
	    
	    board[x][y] = 0;
	    System.out.println(this);
	    
	    
	}
    }
}
public class KnightTour{
    public static void main (String [] args){
	int n = 8;
	try {
	    n = Integer.parseInt(args[0]);
	}
	catch (Exception e){
	    System.out.println(  "Invalid input. Using board size " + n + "..." );
	}
	KnightSolver ms = new KnightSolver(n);
	System.out.println("[2J");
	System.out.println(ms);
	long x = System.currentTimeMillis();
	ms.findTour(2,2,1);
	long y = System.currentTimeMillis();
	System.out.println("" + (y - x));

    }
}
    
    
