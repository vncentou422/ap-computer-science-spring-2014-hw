import java.io.*;
import java.util.*;

class QueenSolver {
    private int[][] board;
    private int sidelength = 20; 
    private boolean full;
    
    final private String QUEEN = "Q";
    final private String VISITED_PATH = ".";
    
    public int[][] getBoard(){
	return board;
    }
    public int[][] populate(){
	for (int x = 1; x < board.length; x++){
	    for (int c = 1; c< board[x].length; c++){
		board[x][c] = 0;
	    }   
	}
	for (int x = 0; x < board.length; x++){
	    board[x][0] = -1;
	    board[x][19] = -1;
	    
	}
	for (int x = 0; x < board.length; x++){
	    board[0][x] = -1;
	    board[19][x] = -1;
	    
	}
    }
	
		
		
    public String toString() {
	//send ANSI code "ESC[0;0H" to place cursor in upper left
	String retStr = "[0;0H";  
	//emacs shortcut: C-q, then press ESC
	//emacs shortcut: M-x quoted-insert, then press ESC
	// (alternatively, just copy and paste from this file...)

	int i, j;
	for (i = 0; i < sideLength + 4; i++){
	    for ( j = 0; j < sideLength+4;j++){
		retStr = retStr + String.format( "%3d" , board[j][i]);
	    }
	    retStr = retStr + "\n";
	}
	/*
	for( i=0; i<h; i++ ) {
	    for( j=0; j<w; j++ )
		retStr = retStr + board[j][i];
	    retStr = retStr + "\n";
	}
	return retStr;
	*/
    }
    public static void print( int[][] a ) {	
	for (int x = 0; x < a.length; x++){
	    String retStr = "";
	    for (int c = 0; c < a[x].length; c++){
	        
		retStr += a[x][c];
	    }
	    if (retStr.length() > 1)
		retStr = retStr.substring(0, retStr.length());
	    System.out.println(retStr);
	}
    }
    public void findTour(int x, int y, int n){
	delay(50);
	if ( n == sideLength * sideLength ){
	    full = true;
	    System.out.println(this);
	    return;
	}
	else if ( board[x][y] == 1 || board[x][y] == -1){
	    return;
	}
	else{
	    board[x][y] =  Queen;
	    System.out.print(this);
	    if ( !full) findTour(x + 1, y, n + 1);
	    if ( !full) findTour(x - 1, y, n + 1);
	    if ( !full) findTour(x , y + 1, n + 1);
	    if ( !full) findTour(x , y - 1, n + 1);
	    board[x][y] = VISITED_PATH;
	    System.out.println(this);
	    return;
	}
    }
    public static void main (String [] args){
	QueenSolver ms = new QueenSolver();
	System.out.println("^[[2J");
	System.out.print(ms);
	
	//print(a);
    }
}
    
    