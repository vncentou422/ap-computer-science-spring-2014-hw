//Vincent Ou
//APCS2 pd 8
//2014-03-10
//HW#11

/*==================================================
  class GuessNumber -- fun fun fun!

  eg, sample interaction with user:
  java GuessNumber
  Guess a number fr 1-100: 50
  Too high
  Guess a number fr 1-49: 25
  Too low
  Guess a number fr 26-49: 38
  Correct! It took 3 guesses
  ==================================================*/
import cs1.Keyboard;
public class GuessNumber {

    //instance vars
    private int _lo, _hi, _guessCtr, _target;


    /*==================================================
      constructor -- initializes a guess-a-number game
      pre:  
      post: _lo is lower bound, _hi is upper bound,
            _guessCtr is 1, _target is random int on range [_lo,_hi]
      ==================================================*/
    public GuessNumber( int a, int b ) {
	_lo = a;
        _hi = b;
        _guessCtr = 1;
        _target = (int) (Math.random()*(b-a +1));
    }


    /*==================================================
      void playRec() -- Prompts a user to guess until guess is correct.
                        Uses recursion.
      ==================================================*/
    public void playRec() {
	String retStr;
        System.out.print("Guess a number from " + _lo+ "-" + _hi + ": ");
        int x = Keyboard.readInt();
        if (x == _target){
            retStr = "Correct! It took " + _guessCtr;          
            if (_guessCtr == 1)
                retStr += " guess.";
            else 
                retStr += " guesses.";        
            System.out.println(retStr);
        }
        else{
           if  (x > _target){
               retStr = "Too high, try again...";
               _hi = x - 1; //sets high value
	       _guessCtr ++;
               System.out.println(retStr);
               playRec();
           }
           else {
               retStr = "Too low, try again...";
               _lo = x + 1; //sets lo value
	       _guessCtr ++;
               System.out.println(retStr);
               playRec();
           }
        }
    }


    /*==================================================
      void playIter() -- Prompts a user to guess until guess is correct.
                         Uses iteration.
      ==================================================*/
    public void playIter() {
	String retStr;
        System.out.print("Guess a number from " + _lo+ "-" + _hi + ": ");
        int x = Keyboard.readInt();
        while (x != _target){
            if (x > _target){
                retStr = "Too high, try again... ";
                _hi = x-1;
                _guessCtr ++;
            }
            else{
                retStr = "Too low, try again... ";
                _lo = x+1;
                _guessCtr ++;
            }
            System.out.println(retStr);
            System.out.print("Guess a number from " + _lo+ "-" + _hi + ": ");
            x = Keyboard.readInt();

        }
        retStr = "Correct! It took " + _guessCtr;
        if (_guessCtr == 1)
            retStr += " guess.";
        else
            retStr += " guesses.";
        System.out.println(retStr);
    }


    //wrapper for playRec/playIter to simplify calling
    public void play() { 
	//use one or the other below:
	//playRec();
	playIter();
    }


    //main method to run it all
    public static void main( String[] args ) {

	//instantiate a new game
	GuessNumber g = new GuessNumber(1,100);

	//start the game
	g.play();
    }

}//end class GuessNumber
