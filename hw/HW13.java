//Vincent Ou
//APCS pd 8
//HW#13
//2014-03-12


/*
Part 0: Compose a function to count the number of trios whose sum is 0, and classify its runtime. Exempli gratia,
                          
int[] foo = { 10, 20, 30, -10, -20, 0 }
...has these trios:
10,-10,0
20,-20,0
30,-10,-20
Part 1: For an n x n 2D array of ints, wherein numbers increase across any row (L->R) and down any column...
e.g.,
| 1  3  5 |
| 3  7  8 |
| 5 12 15 |
Compose a function O(n) search fxn
Part 2: For an array of integers 1 to n...
If 1 num duplicated & 1 num missing,
e.g.,
[ 2, 1, 2, 4 ]  is missing 3
Compose a function to find the missing number in linear ( O(n) ) time and using constant ( O(1) ) additional space.  (Array modification is allowed, but creating a temporary array is not.)
*/
import java.util.*;
import java.io.*;
public class HW13{
    public static int counttrio(int[] x){
	int retNum = 0;
	for (int count = 0; count + 3 < x.length; count++){
	    if ((x[count] + x[count + 1] + x[count + 2]) == 0){
		retNum++;
	    }
	}
	return retNum;
    } // O(n)
    public static String TwoDSearch(int[][] x , int target, int row, int column){
	String retStr = "Not Found";
	if (row == x.length || column == x.length)// || row < 0 || row < 0)
	    return retStr;
	int y = x[row][column];
	if (target == y)
	    retStr = "Found at " + row + "," + column;
	else if (target > y)
	    return TwoDSearch(x, target, row, column + 1);
	else
	    return TwoDSearch(x,target, row -  1, column);
	return retStr;
    }//O(n)
    public static int Missing (int[] x){
	/*
	Arrays.sort(x);
	int retNum = -1;
	for (int counter=0; counter<x.length - 1; counter++){
	    if (x[counter]==x[counter+1] && x[counter+1] != x[counter+2] - 1){
		retNum = x[counter] + 1;
	    }
	}
	*/
	int count = 0;
	int sum = 0;
	int sum2 = 0;
	int retNum = 0;
	int y = (x.length*(x.length + 1))/2;
	int z = (x.length)*(x.length+1) * (1 + 2 * x.length) * (1/6);
	while (count < x.length){
	    sum += x[count];
	    sum2 += x[count] * x[count];
	    count++;
	}
	int diff = y - sum;
	int diff2 = z - sum2;
	int dup = (diff2 - (diff * diff))/(diff * 2);
	int miss = dup + diff;
	return miss; 
    }//I think this is O(n)
	    
    public static void main (String[] args){
	int[] foo = { 10, 20, 30, -10, -20, 0 };
	System.out.println(counttrio(foo));
	int [][] boo = new int [3][3];
	boo [0][0] = 1;
	boo [1][0] = 3;
	boo [2][0] = 5;
	boo [0][1] = 3;
	boo [1][1] = 7;
	boo [2][1] = 12;
	boo [0][2] = 5;
	boo [1][2] = 8;
	boo [2][2] = 15;
	int[][] shoo= new int[5][5];
	shoo[0][0] = 1;
	shoo[0][1] = 3;
	shoo[0][2] = 8;
	shoo[0][3] = 12;
	shoo[0][4] = 16;
	shoo[1][0] = 3;
	shoo[1][1] = 7;
	shoo[1][2] = 9;
	shoo[1][3] = 13;
	shoo[1][4] = 18;
	shoo[2][0] = 5;
	shoo[2][1] = 8;
	shoo[2][2] = 10;
	shoo[2][3] = 15;
	shoo[2][4] = 22;
	shoo[3][0] = 7;
	shoo[3][1] = 10;
	shoo[3][2] = 13;
	shoo[3][3] = 18;
	shoo[3][4] = 26;
	shoo[4][0] = 9;
	shoo[4][1] = 12;
	shoo[4][2] = 15;
	shoo[4][3] = 20;
	shoo[4][4] = 29;
	System.out.println(TwoDSearch(shoo, 15, shoo.length - 1, 0));
	int[] woo ={ 2, 1, 2, 4 };
	//int[] woo ={ 1, 2, 3, 4, 5, 6, 8, 8 };
	System.out.println(Missing(woo));
    }
}
	    
