//Vincent Ou
//pd 9
//HW17
//2014-03-20
/*****************************************************
 * class LList
 * Implements a linked list.
 *****************************************************/

public class LList implements List { //your List.java must be in same dir
    private String[] data;
    
    public boolean add( String x ) { 
	String[] y = new String[data.length + 1];
	y[0] = x;
	for ( int counter = 1; counter < y.length; counter++){
	    y[counter] = data[counter-1];
	}
        return true;
    } 

    public String get( int i ) { 
	return data[i];
    } 
    
    public String set( int i, String x ) {
	String y = data[i];
	data[i] = x;
	return y;
    } 
    
    public int size() { 
	return data.length;
    } 
}
