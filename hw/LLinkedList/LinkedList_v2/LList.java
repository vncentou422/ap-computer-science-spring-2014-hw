/*****************************************************
 * class LList
 * Implements a linked list of LLNodes.
 *****************************************************/

/*
  "Why was all laid to burnination, and what does this reveal about the current state of society?"
  
  Society was meant to be undone by the hands that created it.

*/
public class LList implements List { //your List.java must be in same dir

    //instance vars
    private LLNode _head;
    private int _size;

    // constructor -- initializes instance vars
    public LList( ) {
	_head = null; //at birth, a list has no elements
	_size = 0;
    }


    //--------------v  List interface methods  v--------------
    public boolean add( String newVal ) { 
	LLNode tmp = new LLNode( newVal, _head );
	_head = tmp;
	_size++;
	return true;
    } 

    public void add( int i , String s){
	
	LLNode tmp = _head;
	for(int x = 1; x < i; x++)	
	    tmp = tmp.getNext();
    	LLNode y = new LLNode(s, tmp.getNext());
    	tmp.setNext(y);
    	_size++;
        
	

    }
    public String remove(int i){
	LLNode tmp = _head;
	for(int x = 1; x < i; x++)
 
    		tmp = tmp.getNext();
    	
    	String retString = tmp.getNext().getCargo();
    	tmp.setNext(tmp.getNext().getNext());
	_size--;
    	return retString;
    }
    public String get( int index ) { 

	if ( index < 0 || index >= size() )
	    throw new IndexOutOfBoundsException();

	String retVal;
	LLNode tmp = _head; //create alias to head

	//walk to desired node
	for( int i=0; i < index; i++ )
	    tmp = tmp.getNext();

	//check target node's cargo hold
	retVal = tmp.getCargo();
	return retVal;
    } 


    public String set( int index, String newVal ) { 

	if ( index < 0 || index >= size() )
	    throw new IndexOutOfBoundsException();

	LLNode tmp = _head; //create alias to head

	//walk to desired node
	for( int i=0; i < index; i++ )
	    tmp = tmp.getNext();

	//store target node's cargo
	String oldVal = tmp.getCargo();
	
	//modify target node's cargo
	tmp.setCargo( newVal );
	
	return oldVal;
    } 


    //return number of nodes in list
    public int size() { return _size; } 

    //--------------^  List interface methods  ^--------------


    // override inherited toString
    public String toString() { 
	String retStr = "HEAD->";
	LLNode tmp = _head; //init tr
	while( tmp != null ) {
	    retStr += tmp.getCargo() + "->";
	    tmp = tmp.getNext();
	}
	retStr += "NULL";
	return retStr;
    }


    //main method for testing
    public static void main( String[] args ) {

	LList james = new LList();

	System.out.println( james );
	System.out.println( "size: " + james.size() );

	james.add("beat");
	System.out.println( james );
	System.out.println( "size: " + james.size() );

	james.add("a");
	System.out.println( james );
	System.out.println( "size: " + james.size() );

	james.add("need");
	System.out.println( james );
	System.out.println( "size: " + james.size() );

	james.add("I");
	System.out.println( james );
	System.out.println( "size: " + james.size() );

	System.out.println( "2nd item is: " + james.get(1) );

	james.set( 1, "got" );
	System.out.println( "...and now 2nd item is: " + james.set(1,"got") );

	System.out.println( james );
	
	//-----
	
	james.add(3,"groovy");
	System.out.println( james );
	System.out.println( "size: " + james.size() );
	/*
	james.remove(4);
	james.remove(2);
	System.out.println( james );
	System.out.println( "size: " + james.size() );
	*/

	
    }

}//end class LList



