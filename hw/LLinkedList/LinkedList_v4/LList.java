/*****************************************************
 * class LList
 * Implements a linked list of LLNodes.
 *****************************************************/

/*
  "Why was all laid to burnination, and what does this reveal about the current state of society?"
  
  Society was meant to be undone by the hands that created it.

*/
public class LList<T> implements List<T> { //your List.java must be in same dir

    //instance vars
    private DLLNode _head;
    private int _size;

    // constructor -- initializes instance vars
    public LList( ) {
	_head = null; //at birth, a list has no elements
	_size = 0;
    }


    //--------------v  List interface methods  v--------------
    public boolean add( T newVal ) { 
	DLLNode tmp = new DLLNode( newVal, _head );
	_head = tmp;
	_size++;
	return true;
    } 

    public void add( int i , T s){
	
	DLLNode tmp = _head;
	for(int x = 1; x < i; x++)	
	    tmp = tmp.getNext();
    	DLLNode y = new DLLNode(s, tmp.getNext());
    	tmp.setNext(y);
    	_size++;
        
	

    }
    public T remove(int i){
	DLLNode tmp = _head;
	for(int x = 1; x < i; x++) 
    		tmp = tmp.getNext();
    	T retString = (T)tmp.getNext().getCargo();
    	tmp.setNext(tmp.getNext().getNext());
	_size--;
    	return retString;
    }
    public T get( int index ) { 

	if ( index < 0 || index >= size() )
	    throw new IndexOutOfBoundsException();

	T retVal;
	DLLNode tmp = _head; //create alias to head

	//walk to desired node
	for( int i=0; i < index; i++ )
	    tmp = tmp.getNext();

	//check target node's cargo hold
	retVal = (T)tmp.getCargo();
	return retVal;
    } 


    public T set( int index, T newVal ) { 

	if ( index < 0 || index >= size() )
	    throw new IndexOutOfBoundsException();

	DLLNode tmp = _head; //create alias to head

	//walk to desired node
	for( int i=0; i < index; i++ )
	    tmp = tmp.getNext();

	//store target node's cargo
        T oldVal = (T)tmp.getCargo();
	
	//modify target node's cargo
	tmp.setCargo( newVal );
	
	return oldVal;
    } 


    //return number of nodes in list
    public int size() { return _size; } 

    //--------------^  List interface methods  ^--------------


    // override inherited toString
    public String toString() { 
	String retStr = "HEAD->";
	DLLNode tmp = _head; //init tr
	while( tmp != null ) {
	    retStr += tmp.getCargo() + "->";
	    tmp = tmp.getNext();
	}
	retStr += "NULL";
	return retStr;
    }


    //main method for testing
    public static void main( String[] args ) {

	LList james = new LList();

	System.out.println( james );
	System.out.println( "size: " + james.size() );

	james.add("beat");
	System.out.println( james );
	System.out.println( "size: " + james.size() );

	james.add("a");
	System.out.println( james );
	System.out.println( "size: " + james.size() );

	james.add("need");
	System.out.println( james );
	System.out.println( "size: " + james.size() );

	james.add("I");
	System.out.println( james );
	System.out.println( "size: " + james.size() );

	System.out.println( "2nd item is: " + james.get(1) );

	james.set( 1, "got" );
	System.out.println( "...and now 2nd item is: " + james.set(1,"got") );

	System.out.println( james );
	
	//-----
	
	james.add(3,"groovy");
	System.out.println( james );
	System.out.println( "size: " + james.size() );
	/*
	james.remove(4);
	james.remove(2);
	System.out.println( james );
	System.out.println( "size: " + james.size() );
	*/

	
    }

}//end class LList



