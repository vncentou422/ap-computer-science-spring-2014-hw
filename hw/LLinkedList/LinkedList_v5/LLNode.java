/*****************************************************
 * class LLNode
 * Implements a node, for use in lists and other container classes.
 *****************************************************/

public class LLNode<T> {

    private T _cargo;    //cargo may only be of type T
    private LLNode<T> _nextNode; //pointer to next LLNode
    private LLNode<T> _prevNode;


    // constructor -- initializes instance vars
    public LLNode( T value, LLNode<T> next, LLNode<T> prev ) {
	_cargo = value;
	_nextNode = next;
	_prevNode = prev;
    }


    //--------------v  ACCESSORS  v--------------
    public T getCargo() { return _cargo; }
    public LLNode<T> getPrev() {return _prevNode; }
    public LLNode<T> getNext() { return _nextNode; }
    //--------------^  ACCESSORS  ^--------------


    //--------------v  MUTATORS  v--------------
    public T setCargo( T newCargo ) {
	T foo = getCargo();
	_cargo = newCargo;
	return foo;
    }

    public LLNode<T> setNext( LLNode<T> newNext ) {
	LLNode<T> foo = getNext();
	_nextNode = newNext;
	return foo;
    }

    public LLNode<T> setPrev( LLNode<T> newPrev){
	LLNode<T> foo = getPrev();
	_prevNode = newPrev;
	return foo;
    }
    //--------------^  MUTATORS  ^--------------


    // override inherited toString
    public String toString() { return _cargo.toString(); }


    //main method for testing
    public static void main( String[] args ) {

	//Below is an exercise in creating a linked list...

	//Create a node
	LLNode<String> first = new LLNode<String>( "cat", null, null );

	//Create a new node after the first
	first.setNext( new LLNode<String>( "dog", null , first) );

	//Create a third node after the second
	first.getNext().setNext( new LLNode<String>( "cow", null , first.getNext()) );

	LLNode temp = first; 
	while( temp != null ) {
	    System.out.println( temp );
	    temp = temp.getNext();
	}

    }//end main

}//end class LLNode
