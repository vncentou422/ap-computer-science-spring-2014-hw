//Vincent Ou
//APCS2 pd 8
//HW10
//2014-03-06
//Partner: Cooper Weaver
//TeamName: VCCode

/*=======================================
  The sortV seemed to run faster due to the fact that it was just changing the xisting data storage while the regular sort had to create and return the new changed and sorted array. Time required to run the code increased in a parabolic manner, capping out in extreme runtimes for 1,000,000 integers in the array. SortV in the end, was faster than the regular sort.

  Mean execution times for dataset of size n:
  Batch size: <# of times each dataset size was run>
  n=1       time: 0.068s
  n=10      time: 0.074s
  n=100     time: 0.076s
  n=1000    time: 0.093s
  n=10000   time: 0.428s
  n=100000  time: 40.414s
  n=1000000 time: 1hr+
  sortV
  n=1       time: 0.058s
  n=10      time: 0.060s
  n=100     time: 0.062s
  n=1000    time: 0.073s
  n=10000   time: 0.276s
  n=100000  time: 24.323s
  n=1000000 time: 1hr+

  ======================================*/

import java.util.ArrayList;

public class MergeSort{
    private ArrayList<Integer> array;
    public MergeSort(){
	//creates an array of a random length and add random numbers to it
	array = new ArrayList<Integer>();
	
	

    }
    //populate
    public void populate(int x){
	
	while (x > 0){
	    array.add((Integer)((int)(100*Math.random())));
	    x--;
	}
    }
    //return array
    public ArrayList<Integer> getArray(){
	return array;
    }
    //prints out arrays
    
    public static String toString(ArrayList<Integer> list){
	String retNum = "";
	for (int x = 0; x < list.size(); x++){
	    retNum += list.get(x) + ",";
	}
	return retNum.substring(0,retNum.length() - 1);
    }
    
    public ArrayList<Integer> merge(ArrayList<Integer> x, ArrayList<Integer> y){
	int counterx = 0;
	int countery = 0;
	
	ArrayList<Integer> finalarray = new ArrayList<Integer>();
	//First part of merge that compares both elements in the arrays
	while (counterx < x.size() && countery < y.size()){
	    if (x.get(counterx).compareTo(y.get(countery)) < 0) {
		finalarray.add(x.get(counterx));
		counterx++;
	    }
	    else {
		finalarray.add(y.get(countery));
		countery++;
	    }
	   
	}
	//after one list is exhausted, the remaining list must be added
        while( counterx < x.size()){
	    finalarray.add(x.get(counterx));
	    counterx++;
	}
	 while( countery < y.size()){
	    finalarray.add(y.get(countery));
	    countery++;
	}
	return finalarray;
    }

	    
	

    public ArrayList<Integer> sort(ArrayList<Integer> x){
	//list is sorted when it is one element
	if (x.size() == 1){
	    return x;
	}
	else{
	    //otherwise split it in two and sort them, then merge
	    return merge(sort(new ArrayList<Integer>(x.subList(0,x.size()/2))),
			 sort(new ArrayList<Integer>(x.subList(x.size()/2, x.size()))));
	}
    }
    public void sortV(ArrayList<Integer> x){
	if (x.size() == 1){
	    sort(x);
	}
	else{
	    array = merge(sort(new ArrayList<Integer>(x.subList(0,x.size()/2))),
			 sort(new ArrayList<Integer>(x.subList(x.size()/2, x.size()))));
	}
    }
    public static void main (String[] args){
	int num = Integer.parseInt(args[0]);
	
	MergeSort x = new MergeSort();
	x.populate(num);
	System.out.println(toString((x.sort(x.getArray()))));
    }
}

	    
