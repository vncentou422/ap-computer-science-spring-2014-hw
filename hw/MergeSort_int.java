//Vincent Ou
//APCS2 pd 8
//HW09
//2014-03-05
//Partner: Nicholas Perotti

public class MergeSort{
    private int[] array;
    public MergeSort(){
	//creates an array of a random length and add random numbers to it
	array = new int[(int)(50 * Math.random())];
	for (int x = 0; x < array.length; x++){
	    array[x] = (int)(100 * Math.random());
	}

    }
    //return array
    public int[] getArray(){
	return array;
    }
    //prints out arrays
    public static String toString(int [] list){
	String retNum = "";
	for (int x = 0; x < list.length; x++){
	    retNum += list[x] + ",";
	}
	return retNum.substring(0,retNum.length() - 1);
    }
    public int[] merge(int[] x, int[] y){
	int counterx = 0;
	int countery = 0;
	int counter = 0;
	int[] finalarray = new int[x.length + y.length];
	/*
	  //Vincent's old code
	while(counterx < x.length && countery < y.length){
	    if (x[counterx] <= y[countery]){
		finalarray[counter] = x[counterx];
		counterx++;
	    }
	    else{
		finalarray[counter] = y[countery];
		countery++;
	    }
	    counter++;
	}
	while (counterx < x.length){
	    finalarray[counter] = x[counterx];
	    counter++;
	    counterx++;
	}
	while (countery < y.length){
	    finalarray[counter] = y[countery];
	    counter++;
	    countery++;
	}
	*/
	//changed code
	//First part of merge that compares both elements in the arrays
	while (counterx < x.length && countery < y.length){
	    if (x[counterx] <= y[countery]){
		finalarray[counter] = x[counterx];
		counterx++;
	    }
	    else {
		finalarray[counter] = y[countery];
		countery++;
	    }
	
	    counter++;
	}
	//after one list is exhausted, the remaining list must be added
	if (countery >= y.length){
	    for(; counter < finalarray.length; counter++){
		finalarray[counter] = x[counterx];
		counterx++;
	    }
	}
	else 
	    for(; counter < finalarray.length; counter++){
		finalarray[counter] = y[countery];
		countery++;
	    }
	return finalarray;
    }
	    
	

    public int[] sort(int[] x){
	//list is sorted when it is one element
	if (x.length == 1){
	    return x;
	}
	else{
	    //otherwise split it in two and sort them, then mergex
	    int y = x.length/2;
	    int[] a = new int[y];
	    int[] b = new int[x.length - y];
	    for (int counter = 0; counter < y; counter++){
		a[counter] = x[counter];
	    }
	    for (int counter = 0; counter < x.length - y; counter ++){
		b[counter] = x[counter + y];
	    }
	    return merge(sort(a),sort(b));
	}
    }
    public static void main (String[] args){
	MergeSort x = new MergeSort();
	System.out.println(toString(x.sort(x.getArray())));
    }
}

	    


