//Vincent Ou
//APCS2 pd 8
//HW14
//2014-03-17


public class QuickSelect{
    //Worst case: when the partition index is at the ends
    //Best case? : When the partition index is in the middle
    public static int sort(int[] x, int k, int lo, int hi){
	if (lo >= hi){ 
	    return x[lo];
	}
	int pivot = partition(x, lo, hi);
	int y = pivot - lo + 1; //accounts when you are looking at the right side of the array
	if (lo == k){
	    return x[k];
	}
	else if ( lo > k){
	    return sort(x, k, pivot - 1, hi); //sorts through the lower half
	}
	else
	    return sort(x, k - y, pivot + 1, hi); // sorts through the upper
    }
    
    public static int partition(int[] x, int lo, int hi){
	int pivot = (int)(x.length * Math.random()); //randomly selects pivot 
	int pivotx = x[pivot]; 
	int counter = lo; //needs to know how far you are in the array
	for (int i = lo; i < hi; i++){
	    if (x[i] <= pivotx){ //all values that are less/equal to the pivot
	        swap(x, counter, i); // are shifted towards the left
		counter++; 
	    }
	}
	return counter;
	
		
    }
    public static void swap(int[] x, int y, int z){ //swap method
	int temp = x[y];
	x[y] = x[z];
	x[z] = temp;
    }
   
    public static void main (String [] args){
	int[] x = {7,1,5,12,3};
	System.out.println(print(x));
	System.out.println(sort(x, 5, 0 ,x.length - 1));
    }
}
