//Vincent Ou
//APCS2 pd 8
//HW16
//2014-03-19


/*****************************************************
 * class QuickSort
 * Implements quicksort algo to sort an array of ints in-place
 * 1. Summary of QuickSort algorithm:
 *    -creates a temporary lo and hi
 *    -picks a random pivot and partitions so everything to the left is less than the pivot and everything to the right of the pivot is greater
 *    -run quicksort on the upper and lower half the array
 *  
 * 2a. Worst pivot choice and associated runtime: 
 * Exterior values would be the worse due to the fact that it doesn't rearrange the array correctly. O(n)?
 * 2b. Best pivot choice and associated runtime:
 * The medium value of the array is the best because there will be an equal number of uppers and lowers that will be sorted. O(logn)?
 * 
 * 3. Approach to handling duplicate values in array:
 * To account for duplicates, you must make sure you skip over values that are equal. 
 *****************************************************/

public class QuickSort {

    //--------------v  HELPER METHODS  v--------------
    public static void swap( int x, int y, int[] o ) {
	int tmp = o[x];
	o[x] = o[y];
	o[y] = tmp;
    }

    public static void printArr( int[] a ) {
	for ( int o : a )
	    System.out.print( o + " " );
	System.out.println();
    }

    public static void shuffle( int[] d ) {
	int tmp;
	int swapPos;
	for( int i = 0; i < d.length; i++ ) {
	    tmp = d[i];
	    swapPos = i + (int)( (d.length - i) * Math.random() );
	    swap( i, swapPos, d );
	}
    }

    public static int[] buildArray( int size, int numVals ) {
	int[] retArr = new int[size];
	for( int i = 0; i < retArr.length; i++ )
	    retArr[i] = (int)( numVals * Math.random() );
	return retArr;
    }
    //--------------^  HELPER METHODS  ^--------------



    /*****************************************************
     * void qsort(int[])
     * @param d -- array of ints to be sorted in place
     *****************************************************/
    public static void qsort( int[] d ) { 
	qsHelp(0,d.length-1, d);
    }
    public static void qsHelp( int lo, int hi, int[] d ) { 
	if (d.length == 1)
	    return;
	else{
	    if ( lo >= hi )
		 return;
	    int tmpLo = lo;
	    int tmpHi = hi;
	    int pivot = d[lo + (hi - lo)/2];
	    while( tmpLo <= tmpHi ) {
		while( d[tmpLo] < pivot ) {
		    tmpLo++;
		}
		while( d[tmpHi] > pivot ) {
		    tmpHi--;
		}
		
		if (tmpLo <= tmpHi){
		    swap( tmpLo, tmpHi, d );
		    tmpLo++;
		    tmpHi--;
		}
		
		  
		    
	    }
	    //d[tmpLo] = pivot;
	    qsHelp( lo,tmpHi,d); //tmpLo-1, d );
	    qsHelp( tmpLo, hi, d);//tmpLo+1, hi, d );
	}//end qsHelp
    }

    //main method for testing
    public static void main( String[] args ) {

	
	//get-it-up-and-running, static test case:
	int [] arr1 = {7,1,5,12,3};
	System.out.println("\narr1 init'd to: " );
	printArr(arr1);

	qsort( arr1 );	
       	System.out.println("arr1 after qsort: " );
	printArr(arr1);


	// randomly-generated arrays of n distinct vals
	int[] arrN = new int[10];
	for( int i = 0; i < arrN.length; i++ )
	    arrN[i] = i;
       
	System.out.println("\narrN init'd to: " );
	printArr(arrN);

       	shuffle(arrN);
       	System.out.println("arrN post-shuffle: " );
	printArr(arrN);
	qsort( arrN );
	System.out.println("arrN after partitionNVals: " );
	printArr(arrN);


	//get-it-up-and-running, static test case w/ dupes:
	int [] arr2 = {7,1,5,12,3,7};
	System.out.println("\narr2 init'd to: " );
	printArr(arr2);

	qsort( arr2 );	
       	System.out.println("arr2 after qsort: " );
	printArr(arr2);

	// arrays of randomly generated ints
	int[] arrMatey = new int[20];
	for( int i = 0; i < arrMatey.length; i++ )
	    arrMatey[i] = (int)( 48 * Math.random() );
       
	System.out.println("\narrMatey init'd to: " );
	printArr(arrMatey);

       	shuffle(arrMatey);
       	System.out.println("arrMatey post-shuffle: " );
	printArr(arrMatey);

	qsort( arrMatey );
	System.out.println("arrMatey after partitionNVals: " );
	printArr(arrMatey);
	/*~~~~s~l~i~d~e~~~m~e~~~d~o~w~n~~~~~~~~~~~~~~~~~~~~ (C-kk,C-y) 
	~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~*/

    }//end main

}//end class QuickSort
