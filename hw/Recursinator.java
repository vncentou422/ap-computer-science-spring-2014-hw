//Vincent Ou
//APCS2 pd 8
//HW04
//2014-02-12

/*======================================
  class Recursinator -- an refresher exercise in Java recursion
  ======================================*/


public class Recursinator {

    /*******************************************************
     * int fact(int n)
     * @param n number whose factorial is to be computed
     * precond: n >= 0
     * eg: fact(5) -> 120
     *******************************************************/
    public static int fact( int n ) {
	/*
	int retNum = 1;
	while (n > 0){
	    retNum *= n;
	}
	System.out.println(retNum);
	*/
	if (n == 0)
	    return 1;
	else
	    return (n * fact(n-1));
	
    }


    /*******************************************************
     * int fibRec(int n) returns nth term of Fibonacci sequence
     *     Uses recursion.
     * @param n specifies position of term in sequence.
     * precond: 0th, 1st, 2nd terms are 0,1,1
     * eg: fibRec(3) -> 2
     *******************************************************/
    public static int fibRec(int n) { 
	
	if (n == 0)
	    return 0;
	else if (n == 1 || n == 2)
	    return 1;
	else{
	    int retNum = 0;
	    return retNum += fibRec(n-1) + fibRec(n-2);
	}
	
	
    }


    /*******************************************************
     * int fibIt(int n) returns nth term of Fibonacci sequence
     *     Uses iteratiion.
     * @param n specifies position of term in sequence.
     * precond: 0th, 1st, 2nd terms are 0,1,1
     * eg: fibIt(3) -> 2
     *******************************************************/
    public static int fibIt(int n) { 
	int counter = n;
	int x = 0;
	int y = 1;
	int z = 0;
	while ( counter > 0){
	    x = y;
	    y += z;
	    z = x;
	    counter--;
	}
	return z;
	    
	    
    }


    /*******************************************************
     * String fenceIt(int n) builds a fence with n fenceposts
     * @param n number of |'s in return String
     * precond: n >= 1
     * eg: fenceIt(1) -> "|"
     * eg: fenceIt(2) -> "|---|"
     * eg: fenceIt(3) -> "|---|---|"
     *******************************************************/
    public static String fenceIt( int n ) { 
	if (n == 0){
	    return "";
	}
	else{
	    String retStr = "";
	    for (int x = 1 ; x <= n; x++){
		if (x == 1){
		    retStr += "|";
		}
		else 
		    retStr += "--|";
	    }
	    return retStr;
	}
    }

    /*******************************************************
     * String fenceRec(int n) builds a fence with n fenceposts
     * @param n number of |'s in return String
     * precond: n >= 1
     * eg: fenceRec(1) -> "|"
     * eg: fenceRec(2) -> "|---|"
     * eg: fenceRec(3) -> "|---|---|"
     *******************************************************/
    public static String fenceRec( int n ) { 
	if (n == 0){
	    return "";
	}
	else{
	    String retStr = "|";
	    if ( n > 1){
		int y = n - 1;
		return retStr += "--" + fenceRec(y);
	    }
	    else{
		return retStr;
	    }
	}
    }

	    
	
    


    /*******************************************************
     * String commafyIt(int n) returns a String representation of
     *        n with commas inserted where appropriate
     * @param n number in need of commas
     * precond: n >= 0
     * eg: commafyIt(100) -> 100
     * eg: commafyIt(1000) -> 1,000
     * eg: commafyIt(10,000,000) -> 10,000,000
     *******************************************************/
    public static String commafyIt( int n ) { 
        String retStr = "";
	String num = "" + n;
	if ((n/1000)== 0 ){
	    return num;
	}
	else{
	    for (int y = n; (y/1000) > 0; y /= 1000){
		retStr = num.substring(0,num.length()-3)+","+num.substring(num.length()-3,num.length());
	    }
	    return retStr;
	}
    }


    /*******************************************************
     * String commafyRec(int n) returns a String representation of
     *        n with commas inserted where appropriate
     * @param n number in need of commas
     * precond: n >= 0
     * eg: commafyRec(100) -> 100
     * eg: commafyRec(1000) -> 1,000
     * eg: commafyRec(10,000,000) -> 10,000,000
     *******************************************************/
    public static String commafyRec( int n ) { 
        String retStr = "";
	String num = "" + n;
	if ((n/1000)== 0){
	    return num;
	}
	else {
	    String y = "";
	    if ((n%1000) == 0){
		y+= "000";
	    }
	    else 
		y+= (n%1000);
	    return retStr += commafyRec(n/1000) + "," + y;
	}
    }




    public static void main( String [] args ) {

	/*============================================
	  Main method for testing. Suggested use:
	  Set up a list of test cases for each function 
	  BEFORE you begin writing each method.
	  Move top comment bar (----) down one line at a time
	  when you're ready to test each line.
	  ============================================*/

	
	System.out.println( "fact(0) -> " + fact(0) ); // 1
	System.out.println( "fact(1) -> " + fact(1) ); // 1
	System.out.println( "fact(2) -> " + fact(2) ); // 2
	System.out.println( "fact(3) -> " + fact(3) ); // 6
	System.out.println( "fact(4) -> " + fact(4) ); // 24
	System.out.println( "fact(5) -> " + fact(5) ); // 120
	
	System.out.println( "fibRec(0) -> " + fibRec(0) ); // 0
	System.out.println( "fibRec(1) -> " + fibRec(1) ); // 1
	System.out.println( "fibRec(2) -> " + fibRec(2) ); // 1
	System.out.println( "fibRec(3) -> " + fibRec(3) ); // 2
	System.out.println( "fibRec(4) -> " + fibRec(4) ); // 3
	System.out.println( "fibRec(5) -> " + fibRec(5) ); // 5
	 
	System.out.println( "fibIt(0) -> " + fibIt(0) ); // 0
	System.out.println( "fibIt(1) -> " + fibIt(1) ); // 1
	System.out.println( "fibIt(2) -> " + fibIt(2) ); // 1
	System.out.println( "fibIt(3) -> " + fibIt(3) ); // 2
	System.out.println( "fibIt(4) -> " + fibIt(4) ); // 3
	System.out.println( "fibIt(5) -> " + fibIt(5) ); // 5
	
	System.out.println( "fenceRec(0) -> " + fenceRec(0) ); // ""
	System.out.println( "fenceRec(1) -> " + fenceRec(1) ); // |
	System.out.println( "fenceRec(2) -> " + fenceRec(2) ); // |--|
	System.out.println( "fenceRec(3) -> " + fenceRec(3) ); // |--|--|
	System.out.println( "fenceRec(4) -> " + fenceRec(4) ); // |--|--|--|
	System.out.println( "fenceRec(5) -> " + fenceRec(5) ); // |--|--|--|

	System.out.println( "fenceIt(0) -> " + fenceIt(0) ); // ""
	System.out.println( "fenceIt(1) -> " + fenceIt(1) ); // |
	System.out.println( "fenceIt(2) -> " + fenceIt(2) ); // |--|
	System.out.println( "fenceIt(3) -> " + fenceIt(3) ); // |--|--|
	System.out.println( "fenceIt(4) -> " + fenceIt(4) ); // |--|--|--|
	System.out.println( "fenceIt(5) -> " + fenceIt(5) ); // |--|--|--|

	System.out.println( "commafyIt(0) -> " + commafyIt(0) ); // 0
	System.out.println( "commafyIt(1) -> " + commafyIt(10) ); // 10
	System.out.println( "commafyIt(2) -> " + commafyIt(200) ); // 200
	System.out.println( "commafyIt(3) -> " + commafyIt(3000) ); // 3,000
	System.out.println( "commafyIt(4) -> " + commafyIt(40000) ); // 40,000
	System.out.println( "commafyIt(5) -> " + commafyIt(500000) ); // 500,000
	
	System.out.println( "commafyRec(0) -> " + commafyRec(0) ); // 0
	System.out.println( "commafyRec(1) -> " + commafyRec(10) ); // 10
	System.out.println( "commafyRec(2) -> " + commafyRec(200) ); // 200
	System.out.println( "commafyRec(3) -> " + commafyRec(3000) ); // 3,000
	System.out.println( "commafyRec(4) -> " + commafyRec(40000) ); // 40,000
	System.out.println( "commafyRec(5) -> " + commafyRec(500000) ); // 500,000

    }//end main

}//end class Recursinator
