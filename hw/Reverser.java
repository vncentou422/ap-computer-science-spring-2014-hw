//Vincent Ou
//APCS2 pd 8
//HW03
//2014-02-10

public class Reverser{
    public static void main( String[] args){
	String retStr = "";
	for (String w: args){
	    String retStrr = "";
	    int x = w.length();
	    while (x > 0){
		retStrr += w.substring(x-1,x);
		x--;
	    }
	    retStr += retStrr + " ";
	}
	System.out.println(retStr);
    }
}
