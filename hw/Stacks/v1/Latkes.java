//Vincent Ou
//APCS2pd8
//HW#24
//2013-04-03

public class Latkes{
    private String[] _stack;
    private int _stackSize;
    
    //constructor
    public Latkes(int size){
	_stack = new String[size];
	_stackSize = 0;
    }
    
    //means of insertion
    public void push (String s){
	if (!isFull()){
	    _stack[_stackSize] = s;
	    _stackSize++;
	}
	else{
	    String[] temp = new String[_stack.length + 10];
	    for(int x = 0; x< _stack.length; x++)
		temp[x] = _stack[x];
	    temp[_stack.length + 1] = s;
	    _stack = temp;
	}
       
    }
    
    //means of removal
    public String pop(){
	//_stackSize--;
	String retStr = "";
	if (isEmpty())
	    return null;
	//retStr = _stack[_stackSize-- -1];
	//equivalent too
	retStr = _stack[_stackSize - 1];
	_stackSize--;
	return retStr;
    }
    
    //chk for emptiness
    public boolean isEmpty(){
	return(_stackSize == 0);
    }
    
    //chk for fullness
    public boolean isFull(){
	return (_stackSize == _stack.length);
    }
    
}//end of class
