//Vincent Ou
//APCS2pd8
//HW#26
//2013-04-08

public class Latkes<P>{
    private P[] _stack;
    private int _stackSize;
    
    //constructor
    public Latkes(int size){
	_stack = (P[])new Object[size];
	_stackSize = 0;
    }
    
    //means of insertion
    public void push (P s){
	if (!isFull()){
	    _stack[_stackSize] = s;
	    _stackSize++;
	}
	else{
	    P[] temp = (P[])new P[_stack.length * 2];
	    for(int x = 0; x< _stack.length; x++)
		temp[x] = _stack[x];
	    temp[_stack.length + 1] = s;
	    _stack = temp;
	}
       
    }
    
    //means of removal
    public P pop(){
	//_stackSize--;
        P retStr = "";
	if (isEmpty())
	    return null;
	//retStr = _stack[_stackSize-- -1];
	//equivalent too
	retStr = _stack[_stackSize - 1];
	_stackSize--;
	return retStr;
    }
    
    //chk for emptiness
    public boolean isEmpty(){
	return(_stackSize == 0);
    }
    
    //chk for fullness
    public boolean isFull(){
	return (_stackSize == _stack.length);
    }
    
    public P peek(){
	if (isEmpty())
	    return null;
	return _stack[_stackSize - 1];
}//end of class
