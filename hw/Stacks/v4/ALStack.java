//Vincent Ou
//APCS2pd8
//HW#27
//2013-04-09

import java.util.*;
import java.io.*;

public class ALStack<P> implements Stack<P>{
    private ArrayList<P> _stack;
    private int _stackSize;
    
    //constructor
    public ALStack(int size){
	_stack = new ArrayList<P>();
	_stackSize = 0;
    }
    
    //means of insertion
    public void push (P s){
	/*
	if (!isFull()){
	    _stack[_stackSize] = s;
	    _stackSize++;
	}
	else{
	    P[] temp = (P[])new P[_stack.length * 2];
	    for(int x = 0; x< _stack.length; x++)
		temp[x] = _stack[x];
	    temp[_stack.length + 1] = s;
	    _stack = temp;
	}
	*/
	_stack.add(_stackSize,s);
	_stackSize++;
       
    }
    
    //means of removal
    public P pop(){
	//_stackSize--;
	// P retStr = "";
	if (isEmpty())
	    return null;
	//retStr = _stack[_stackSize-- -1];
	//equivalent too
	P retStr = _stack.get(_stackSize - 1);
	_stackSize--;
	return retStr;
    }
    
    //chk for emptiness
    public boolean isEmpty(){
	return(_stackSize == 0);
    }
    
    //chk for fullness
    public boolean isFull(){
	return (_stackSize == _stack.size());
    }
    
    public P peek(){
	if (isEmpty())
	    return null;
	return _stack.get(_stackSize - 1);
    }
     public static void main( String[] args ) {

	ALStack tastyStack = new ALStack(10);

	tastyStack.push("aoo");
	tastyStack.push("boo");
	tastyStack.push("coo");
	tastyStack.push("doo");
	tastyStack.push("eoo");
	tastyStack.push("foo");
	tastyStack.push("goo");
	tastyStack.push("hoo");
	tastyStack.push("ioo");
	tastyStack.push("joo");
	tastyStack.push("coocoo");
	tastyStack.push("cachoo");

	//cachoo
	System.out.println( tastyStack.pop() );
	//coocoo
	System.out.println( tastyStack.pop() );
	//joo
	System.out.println( tastyStack.pop() );
	//ioo
	System.out.println( tastyStack.pop() );
	//hoo
	System.out.println( tastyStack.pop() );
	//goo
	System.out.println( tastyStack.pop() );
	//foo
	System.out.println( tastyStack.pop() );
	//eoo
	System.out.println( tastyStack.pop() );
	//doo
	System.out.println( tastyStack.pop() );
	//coo
	System.out.println( tastyStack.pop() );
	//boo
	System.out.println( tastyStack.pop() );
	//aoo
	System.out.println( tastyStack.pop() );

	//stack empty by now; SOP(null)
	System.out.println( tastyStack.pop() );
    }
}//end of class
