//Vincent Ou
//APCS2pd8
//HW#28
//2013-04-10


import java.util.*;
import java.io.*;

public class LLStack<P> implements Stack<P>{
    private LinkedList<P> _stack;
    // private int _stackSize;
    
    //constructor
    public LLStack(){
	_stack = new LinkedList<P>();
	//_stackSize = 0;
    }
    
    //means of insertion
    public void push (P s){
	_stack.add(s);
	//_stackSize++;
       
    }
    
    //means of removal
    public P pop(){
	//_stackSize--;
	// P retStr = "";
	if (isEmpty())
	    return null;
	else{
	    //_stackSize--;
	    return _stack.removeLast();
	}

    }
    public P popfirst(){
	if (isEmpty())
	    return null;
	else
	    return _stack.removeFirst();
    }
    //chk for emptiness
    public boolean isEmpty(){
	return(_stack.size() == 0);
    }
    /*
    //chk for fullness
    public boolean isFull(){
	return (_stackSize == _stack.size());
    }
    */
    public P peek(){
	if (isEmpty())
	    return null;
	return _stack.getLast();
    }
    public P peekfirst(){
	if (isEmpty())
	    return null;
	return _stack.getFirst();
    }
     public static void main( String[] args ) {

	LLStack tastyStack = new LLStack();

	tastyStack.push("aoo");
	tastyStack.push("boo");
	tastyStack.push("coo");
	tastyStack.push("doo");
	tastyStack.push("eoo");
	tastyStack.push("foo");
	tastyStack.push("goo");
	tastyStack.push("hoo");
	tastyStack.push("ioo");
	tastyStack.push("joo");
	tastyStack.push("coocoo");
	tastyStack.push("cachoo");

	//cachoo
	System.out.println( tastyStack.pop() );
	//coocoo
	System.out.println( tastyStack.pop() );
	//joo
	System.out.println( tastyStack.pop() );
	//ioo
	System.out.println( tastyStack.pop() );
	//hoo
	System.out.println( tastyStack.pop() );
	//goo
	System.out.println( tastyStack.pop() );
	//foo
	System.out.println( tastyStack.pop() );
	//eoo
	System.out.println( tastyStack.pop() );
	//doo
	System.out.println( tastyStack.pop() );
	//coo
	System.out.println( tastyStack.pop() );
	//boo
	System.out.println( tastyStack.pop() );
	//aoo
	System.out.println( tastyStack.pop() );

	//stack empty by now; SOP(null)
	System.out.println( tastyStack.pop() );
    }
}//end of class
