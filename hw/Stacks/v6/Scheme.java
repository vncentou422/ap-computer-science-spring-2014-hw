//Vincent Ou
//APCS2 pd 8
//HW29
//2014-04-20

/******************************************************
 * ALGORITHM for EVALUATING A SCHEME EXPRESSION:
 * I would first get rid of all the spaces first. I would then parse the String and find
any open parentheses and if i find one i would increase a counter. Otherwise I would push
the element onto a stack. Once I found the proper closing parentheses, I would run
a recursive call on that section of the String, thus simplifying it down. After I have only
single elements in my Stack, I would evaluate it based on the operation I had recorded in 
the beginning of the function. 
 * STACK OF CHOICE: Linked List by Vincent b/c I decided to make another function
that would pop off the first element in order to successfully divide.
 ******************************************************/
import java.util.*;
import java.io.*;
public class Scheme{
    public static String solve(String input){
	
	LLStack<String> data = new LLStack<String>();
	int answer = 0;
	int counter = 0;	
	String replacement = "";
	for (int x = 0; x < input.length(); x++){
	    if (!input.substring(x,x+1).equals(" ")){
		
		replacement += input.substring(x,x+1);
	    }
	}
        String operation = replacement.substring(1,2);
	
	for (int x = 2; x < replacement.length() - 1; x++){
	    if (replacement.substring(x,x+1).equals("(")){
		int y = x;
	        for (int z = x;z< replacement.length(); z++){
		    if (replacement.substring(z,z+1).equals("(")){
			counter++;
		    }
		    else if (replacement.substring(z,z+1).equals(")") && counter == 1){
			y = z;
		        z = replacement.length();
			
		    }
		    else if(replacement.substring(z,z+1).equals(")")&& counter != 1){
			counter--;

		    }
		}
		    
		
		data.push(solve(replacement.substring(x,y+1)));
		x = y;
	    }
	    else
		data.push(replacement.substring(x,x+1));
	}
	if(operation.equals("+"))
	    while(!data.isEmpty())
		answer += Integer.parseInt(data.pop());
	if(operation.equals("-"))
	    while(!data.isEmpty())
		answer -= Integer.parseInt(data.pop());
	if(operation.equals("*")){
	    answer = Integer.parseInt(data.pop());
	    while(!data.isEmpty())
		answer *= Integer.parseInt(data.pop());
	}
	if(operation.equals("/")){
	    answer = Integer.parseInt(data.popfirst());
	    while(!data.isEmpty())
		answer /= Integer.parseInt(data.pop());
	}
	return "" + answer ;

		
	
    }
    public static void main (String [] args){
	System.out.println(solve("( + 1 1 2 )"));
	System.out.println(solve("( - 2 6 )"));
	System.out.println(solve("( * 5 6 9 )"));
	System.out.println(solve("( / 6 2 )"));
	System.out.println(solve("( + 1 2 ( * 1 2 ) )"));
	System.out.println(solve("( + 1 2 ( + 1 2 ) 8 )"));

	
    }
}

