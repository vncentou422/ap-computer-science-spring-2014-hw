//Vincent Ou
//pd 8
//HW40
//2014-05-16



/*****************************************************
 * class ALHeap  ---  skeleton
 * Implements a min heap using an ArrayList as underlying container
 * 
 * TASK:
 * Implement all methods
 * Add helpers as necessary
 * Classify runtime of each method
 *****************************************************/

import java.util.ArrayList;


public class ALHeap {

    //instance vars
    private ArrayList<Integer> _heap; //underlying container
    

    /*****************************************************
     * default constructor  ---  inits empty heap
     *****************************************************/
    public ALHeap() 
    {
	_heap = new ArrayList<Integer>();
    }//O(1)



    /*****************************************************
     * boolean isEmpty()
     * Returns true if no meaningful elements in heap, false otherwise
     *****************************************************/
    public boolean isEmpty() 
    {
	return (_heap.size() == 0);
    }//O(1)



    /*****************************************************
     * add(Integer) 
     * Inserts an element in the heap
     * Postcondition: Tree maintains heap property.
     *****************************************************/
    public void add( Integer addVal ) 
    {
	if (isEmpty())
	    _heap.add(addVal);
	else{
	    _heap.add(addVal);
	    int x = _heap.size()-1;
	    while(x > 1 && _heap.get(getParentIndex(x)) > addVal){
		swap(getParentIndex(x), x);
		x = getParentIndex(x);
	    }
	}
    }//O(logn)



    /*****************************************************
      * Integer peekMin()
      * Returns min value in heap
      * Postcondition: Heap remains unchanged.
      *****************************************************/
    public Integer peekMin() 
    {
	if (!isEmpty())
	    return _heap.get(0);
	return null;
    }//O(1)



    /*****************************************************
     * removeMin()  ---  means of removing an element from heap
     * Removes and returns least element in heap.
     * Postcondition: Tree maintains heap property.
     *****************************************************/
    public Integer removeMin() 
    {
        int retNum = _heap.get(0);
	int switchy = _heap.remove(_heap.size()-1);
	_heap.set(0, switchy);
	int index = 0;
	while (_heap.get(index) > _heap.get(minChildPos(index))){
	    swap(index,minChildPos(index));
	    
	}	  	
	return retNum; 
   
    }//O(?)



    /*****************************************************
     * minChildPos(int)  ---  helper fxn for removeMin()
     * Returns index of least child, or 
     * -1 if no children, or if input pos is not in ArrayList
     * Postcondition: Tree unchanged
     *****************************************************/
    private int minChildPos( int pos ) 
     {
	 
	 
	 if (pos>_heap.size() || (getLeftChildIndex(pos) + 1 > _heap.size() && getRightChildIndex(pos) + 1  > _heap.size()))//(x.equals(null) && y.equals(null)))
	     return -1;
	 
	 else if(getLeftChildIndex(pos)+1 >= _heap.size())
	     return getRightChildIndex(pos)+ 1;
	 
	 else if(getRightChildIndex(pos)+1 >= _heap.size())
	     return getLeftChildIndex(pos)+ 1;
	 
	 else if(_heap.get(getLeftChildIndex(pos)+ 1) < _heap.get(getRightChildIndex(pos)+ 1))
	     return getLeftChildIndex(pos)+ 1;
	 else 
	     return getRightChildIndex(pos)+ 1;
     }//O(1)



    //~~~~~~~~~~~~~~~v~  MISC HELPERS ~v~~~~~~~~~~~~~~~

    // perhaps a SWAPPER... ?
    
    // perhaps a minOf(Integer,Integer)... ?

    public void swap (int x, int y){
	int tmp = _heap.get(x);
	_heap.set(x, _heap.get(y));
	_heap.set(y, tmp);
    }
    
    private int getLeftChildIndex(int index){
	return 2 * index;
    }
    private int getRightChildIndex(int index){
	return 2 * index + 1;
    }
    private int getParentIndex(int index){
	return (index-1)/2;
    }
    
    //~~~~~~~~~~~~~~~^~  MISC HELPERS ~^~~~~~~~~~~~~~~~



    /*****************************************************
     * toString()  ---  overrides inherited method
     * Returns either 
     * a) a level-order traversal of the tree (simple version)
     * b) ASCII representation of the tree (bit more complicated, much more fun)
     *****************************************************/
    public String toString() 
    {
	String retStr = "";
	for(int x:_heap){
	    retStr += x + " ";
	}
	return retStr;
    }//O(?)






    //main method for testing
    public static void main( String[] args ) {

	ALHeap pile = new ALHeap();

	pile.add(2);
	System.out.println(pile);
	pile.add(4);
	System.out.println(pile);
	pile.add(6);
	System.out.println(pile);
	pile.add(8);
	System.out.println(pile);
	pile.add(10);
	System.out.println(pile);
	pile.add(1);
	System.out.println(pile);
	pile.add(3);
	System.out.println(pile);
	pile.add(5);
	System.out.println(pile);
	pile.add(7);
	System.out.println(pile);
	pile.add(9);
	System.out.println(pile);

	
	System.out.println(pile.minChildPos(0));
	System.out.println(pile.minChildPos(2));
	System.out.println(pile.minChildPos(3));
	System.out.println(pile.minChildPos(6));
	System.out.println(pile.minChildPos(4));
	
	
	System.out.println("removing " + pile.removeMin() + "...");
	System.out.println(pile);
	
	System.out.println("removing " + pile.removeMin() + "...");
	System.out.println(pile);
	System.out.println("removing " + pile.removeMin() + "...");
	System.out.println(pile);
	System.out.println("removing " + pile.removeMin() + "...");
	System.out.println(pile);
	System.out.println("removing " + pile.removeMin() + "...");
	System.out.println(pile);
	System.out.println("removing " + pile.removeMin() + "...");
	System.out.println(pile);
	System.out.println("removing " + pile.removeMin() + "...");
	System.out.println(pile);
	System.out.println("removing " + pile.removeMin() + "...");
	System.out.println(pile);
	System.out.println("removing " + pile.removeMin() + "...");
	System.out.println(pile);
	System.out.println("removing " + pile.removeMin() + "...");
	System.out.println(pile);
	System.out.println("removing " + pile.removeMin() + "...");
	System.out.println(pile);
	
    }//end main()

}//end class ALHeap
