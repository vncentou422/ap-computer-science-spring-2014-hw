//Vincent Ou
//APCS pd 8
//HW41
//2014-05-19


public class Heapsort{
    
    private ALHeap alheap;
    
    public Heapsort(){
	alheap = new ALHeap();
    }//O(1)
    
    public Integer[] sort(Integer[] data){
	for(int x =0; x< data.length; x++){
	    alheap.add(data[x]);
	}
	Integer[] retArr = new Integer[data.length];
	for(int x = 0; x < data.length; x++){
	    retArr[x] = alheap.removeMin();
	}
	return retArr;
    }//O(nlogn)
    public static String toString(Integer[] x){
	String retStr = "";
	for (int y = 0; y < x.length; y++){
	    retStr += x[y] + " ";
	}
	return retStr;
    }//O(n)
    public static Integer[] randomizer(){
	Integer[] x = new Integer[100];
	for (int y = 0; y < x.length; y++){
	    x[y] = (int)(Math.random() * 100);
	}
	return x;
    }
    public static void main(String[] arg){
	Heapsort Sorty = new Heapsort();
	Integer[] x = {20,15,36,4,6,8,2,10,16,45};
	Integer[] y = randomizer();
		   
		      
		   
	
	System.out.println(toString(Sorty.sort(x)));
	System.out.println(toString(Sorty.sort(y)));
    }
}