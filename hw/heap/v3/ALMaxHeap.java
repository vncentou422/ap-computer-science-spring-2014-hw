//Vincent Ou
//pd 8
//HW40
//2014-05-16



/*****************************************************
 * class ALHeap  ---  skeleton
 * Implements a max heap using an ArrayList as underlying container
 * 
 * TASK:
 * Implement all methods
 * Add helpers as necessary
 * Classify runtime of each method
 *****************************************************/

import java.util.ArrayList;


public class ALMaxHeap {

    //instance vars
    private ArrayList<Integer> _heap; //underlying container
    

    /*****************************************************
     * default constructor  ---  inits empty heap
     *****************************************************/
    public ALMaxHeap() 
    {
	_heap = new ArrayList<Integer>();
    }//O(1)



    /*****************************************************
     * boolean isEmpty()
     * Returns true if no meaningful elements in heap, false otherwise
     *****************************************************/
    public boolean isEmpty() 
    {
	return (_heap.size() == 0);
    }//O(1)



    /*****************************************************
     * add(Integer) 
     * Inserts an element in the heap
     * Postcondition: Tree maintains heap property.
     *****************************************************/
    public void add( Integer addVal ) 
    {
	if (isEmpty())
	    _heap.add(addVal);
	else{
	    _heap.add(addVal);
	    int x = _heap.size()-1;
	    while(x > 0 && _heap.get(getParentIndex(x)) < addVal){
		swap(getParentIndex(x), x);
		x = getParentIndex(x);
	    }
	}
    }//O(logn)



    /*****************************************************
      * Integer peekMax()
      * Returns max value in heap
      * Postcondition: Heap remains unchanged.
      *****************************************************/
    public Integer peekMax() 
    {
	if (!isEmpty())
	    return _heap.get(0);
	return null;
    }//O(1)



    /*****************************************************
     * removeMax()  ---  means of removing an element from heap
     * Removes and returns least element in heap.
     * Postcondition: Tree maintains heap property.
     *****************************************************/
    public Integer removeMax() 
    {
	//Mr.Brown's version
	if ( _heap.size() == 0 ) 
	    return null;

	//store root value for return at end of fxn
	Integer retVal = peekMax();

	//store val about to be swapped into root
	Integer foo = _heap.get( _heap.size() - 1);

	//swap last (rightmost, deepest) leaf with root
	swap( 0, _heap.size() - 1 );

	//lop off last leaf
	_heap.remove( _heap.size() - 1);

	// walk the now-out-of-place root node down the tree...
	int pos = 0;
	int maxChildPos;

	while( pos < _heap.size() ) {

	    //choose child w/ max value, or check for child
	    maxChildPos = maxChildPos(pos);

	    //if no children, then i've walked far enough
	    if ( maxChildPos == -1 ) 
		break;
	    //if i am less than my least child, then i've walked far enough
	    else if ( foo.compareTo( _heap.get(maxChildPos) ) >= 0 ) 
		break;
	    //if i am > least child, swap with that child
	    else {
		swap( pos, maxChildPos );
		pos = maxChildPos;
	    }
	}
	//return removed value
	return retVal;
   
    }//O(?)



    /*****************************************************
     * maxChildPos(int)  ---  helper fxn for removeMax()
     * Returns index of least child, or 
     * -1 if no children, or if input pos is not in ArrayList
     * Postcondition: Tree unchanged
     *****************************************************/
    private int maxChildPos( int pos ) 
     {
	 
	 int retVal;
	int lc = 2*pos + 1; //index of left child
	int rc = 2*pos + 2; //index of right child

	//pos is not in the heap or pos is a leaf position
	if ( pos < 0 || pos >= _heap.size() || lc >= _heap.size() )
	    retVal = -1;
	//if no right child, then left child is only option for max
	else if ( rc >= _heap.size() )
	    retVal = lc;
	//have 2 children, so compare to find least 
	else if ( _heap.get(lc).compareTo(_heap.get(rc)) > 0 )
	    retVal = lc;
	else
	    retVal = rc;
	return retVal;
     }//O(1)



    //~~~~~~~~~~~~~~~v~  MISC HELPERS ~v~~~~~~~~~~~~~~~

    // perhaps a SWAPPER... ?
    
    // perhaps a maxOf(Integer,Integer)... ?

    public void swap (int x, int y){
	int tmp = _heap.get(x);
	_heap.set(x, _heap.get(y));
	_heap.set(y, tmp);
    }
    
    private int getLeftChildIndex(int index){
	return 2 * index;
    }
    private int getRightChildIndex(int index){
	return 2 * index + 1;
    }
    private int getParentIndex(int index){
	return (index-1)/2;
    }
    

    public int getSize() {
	return _heap.size();
    }
    //~~~~~~~~~~~~~~~^~  MISC HELPERS ~^~~~~~~~~~~~~~~~



    /*****************************************************
     * toString()  ---  overrides inherited method
     * Returns either 
     * a) a level-order traversal of the tree (simple version)
     * b) ASCII representation of the tree (bit more complicated, much more fun)
     *****************************************************/
    public String toString() 
    {
	String retStr = "";
	for(int x:_heap){
	    retStr += x + " ";
	}
	return retStr;
    }//O(?)






    //main method for testing
    public static void main( String[] args ) {

	ALMaxHeap pile = new ALMaxHeap();

	pile.add(2);
	System.out.println(pile);
	pile.add(4);
	System.out.println(pile);
	pile.add(6);
	System.out.println(pile);
	pile.add(8);
	System.out.println(pile);
	pile.add(10);
	System.out.println(pile);
	pile.add(1);
	System.out.println(pile);
	pile.add(3);
	System.out.println(pile);
	pile.add(5);
	System.out.println(pile);
	pile.add(7);
	System.out.println(pile);
	pile.add(9);
	System.out.println(pile);

	
	System.out.println(pile.maxChildPos(0));
	System.out.println(pile.maxChildPos(2));
	System.out.println(pile.maxChildPos(3));
	System.out.println(pile.maxChildPos(6));
	System.out.println(pile.maxChildPos(4));
	
	
	System.out.println("removing " + pile.removeMax() + "...");
	System.out.println(pile);
	
	System.out.println("removing " + pile.removeMax() + "...");
	System.out.println(pile);
	System.out.println("removing " + pile.removeMax() + "...");
	System.out.println(pile);
	System.out.println("removing " + pile.removeMax() + "...");
	System.out.println(pile);
	System.out.println("removing " + pile.removeMax() + "...");
	System.out.println(pile);
	System.out.println("removing " + pile.removeMax() + "...");
	System.out.println(pile);
	System.out.println("removing " + pile.removeMax() + "...");
	System.out.println(pile);
	System.out.println("removing " + pile.removeMax() + "...");
	System.out.println(pile);
	System.out.println("removing " + pile.removeMax() + "...");
	System.out.println(pile);
	System.out.println("removing " + pile.removeMax() + "...");
	System.out.println(pile);
	System.out.println("removing " + pile.removeMax() + "...");
	System.out.println(pile);
	
    }//end main()

}//end class ALHeap
