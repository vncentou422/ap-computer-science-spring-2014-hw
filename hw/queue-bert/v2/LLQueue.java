//Vincent Ou
//APCS2 pd 8
//2014-04-23
//HW#30

/*****************************************************
 * class LLQueue
 * uses a linked list to implement a QUEUE
 * (a collection with FIFO property)
 *
 *       -------------------------------
 *   end |  --->   Q U E U E   --->    | front
 *       -------------------------------
 *
 ******************************************************/

public class LLQueue<T> implements Queue<T> {

    private LLNode<T> _front, _end;

    // default constructor creates an empty queue
    public LLQueue() { 
	/*
	_end = new LLNode<T>(null,null);
	_front = new LLNode<T>(null,_end);
	*/
	_front = _end = null;
    }

    // means of adding a thing to the collection
    public void enqueue( T enQVal ) {
	/*
	if (isEmpty())
	    _front.setValue(enQVal);
	    
	else if (_front.getNext().getValue() == null)
	    _end.setValue(enQVal);
	else{
	
	     LLNode<T> x = new LLNode<T>(enQVal,null);
	     _end.setNext(x);
	     }
	*/
	if (_front == null) {
	    _front = new LLNode <T> (enQVal, null);
	    _end = _front;
	}
	else {
	    LLNode <T> x = new LLNode<T> (enQVal,null);
	    _end.setNext(x);
	    _end = x;
	}
    }//O(1)


    // means of removing a thing from the collection
    // Remove and return thing at front of queue.
    // Assume _queue ! empty.
    public T dequeue() { 
	T x = _front.getValue();
	_front = _front.getNext();
	return x;
    }//O(1)


    // means of peeking at thing next in line for removal
    public T peekFront() {
	return _front.getValue();
    }//O(1)


    public boolean isEmpty() {
	//	if (_front.getValue() == null && _end.getValue() == null)
	if (_front == null)
	    return true;
	else 
	    return false;
    }//O(1)


    // print each node, separated by spaces
    public String toString() { 
	String foo = "";
	LLNode<T> tmp = _front;
	while ( tmp != null ) {
	    foo += tmp.getValue() + " ";
	    tmp = tmp.getNext();
	}
	return foo;
    }//O(n)



    public static void main( String[] args ) {

	Queue<String> LLQueuelJ = new LLQueue<String>();

	System.out.println("\nnow enqueuing thrice..."); 
	LLQueuelJ.enqueue("James");
	LLQueuelJ.enqueue("Todd");
	LLQueuelJ.enqueue("Smith");

	System.out.println("\nnow testing toString()..."); 
	System.out.println( LLQueuelJ ); //for testing toString()...

	System.out.println("\nnow dequeuing thrice..."); 
	System.out.println( LLQueuelJ.dequeue() );
	System.out.println( LLQueuelJ.dequeue() );
	System.out.println( LLQueuelJ.dequeue() );

	System.out.println("\nDequeuing from empty queue should yield error..."); 
	System.out.println( LLQueuelJ.dequeue() );

    }//end main

}//end class LLQueue
