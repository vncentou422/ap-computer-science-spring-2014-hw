//Vincent Ou
//APCS2 pd 8
//2014-04-25
//HW#32a

/*
To create DeQue I added three functions to Queue.java and changed the names of three functions. Enqueueback will add a element to the back of the list, while enqueuefront will add to the front. Dequeueback will remove the back element while dequeuefront will remove the first. Peekfront and peekback looks at its respective elements at where it is located.
*/

public class DeQue<T> implements Queue<T> {

    private DLLNode<T> _front, _end;

    // default constructor creates an empty queue
    public DeQue() { 
	_front = _end = null;
    }
    //O(1)
    
    // means of adding a thing to the collection
    public void enqueueBack( T enQVal ) {
	if ( isEmpty() ) {
	    _front = _end = new DLLNode<T>( enQVal, null, null );
	}
	else {
	    _end.setNext( new DLLNode<T>( enQVal, _end, null ) );
	    _end = _end.getNext();
	}
    }//O(1)
    
    public void enqueueFront( T enQVal ) {
	if ( isEmpty() ) {
	    _front = _end = new DLLNode<T>( enQVal, null, null );
	}
	else {
	    _front.setPrev( new DLLNode<T>( enQVal, null, _front ) );
	    _front = _front.getPrev();
	}
    }//O(1)
    
    public T dequeueBack() { 
	T foo = _end.getValue();
	_end = _end.getPrev();
	//check for emptiness
	if ( _front == null )
	    _end = null;
	return foo;
    }//O(1)
    
    public T dequeueFront() { 
	T foo = _front.getValue();
	_front = _front.getNext();
	//check for emptiness
	if ( _front == null )
	    _end = null;
	return foo;
    }//O(1)

    // means of peeking at thing next in line for removal
    public T peekFront() {
	return _front.getValue();
    }//O(1)
     public T peekBack() {
	return _end.getValue();
    }//O(1)


    public boolean isEmpty() {
	return _front == null; 
    }//O(1)


    // print each node, separated by spaces
    public String toString() { 
	String foo = "";
	DLLNode<T> tmp = _front;
	while ( tmp != null ) {
	    foo += tmp.getValue() + " ";
	    tmp = tmp.getNext();
	}
	return foo;
    }//O(n)



    public static void main( String[] args ) {

	Queue<String> DeQuelJ = new DeQue<String>();

	System.out.println("\nnow enqueuing thrice..."); 
	/*
	DeQuelJ.enqueueBack("James");
	DeQuelJ.enqueueBack("Todd");
	DeQuelJ.enqueueBack("Smith");
	*/	
	DeQuelJ.enqueueFront("James");
	DeQuelJ.enqueueFront("Todd");
	DeQuelJ.enqueueFront("Smith");

	System.out.println("\nnow testing toString()..."); 
	System.out.println( DeQuelJ ); //for testing toString()...

	System.out.println("\nnow dequeuing thrice...");	
	/*
	System.out.println( DeQuelJ.dequeueFront() );
	System.out.println( DeQuelJ.dequeueFront() );
	System.out.println( DeQuelJ.dequeueFront() );
	*/
	System.out.println( DeQuelJ.dequeueBack() );
	System.out.println( DeQuelJ.dequeueBack() );
	System.out.println( DeQuelJ.dequeueBack() );
	System.out.println("\nDequeuing from empty queue should yield error..."); 
	System.out.println( DeQuelJ.dequeueBack() );

    }//end main

}//end class DeQue
